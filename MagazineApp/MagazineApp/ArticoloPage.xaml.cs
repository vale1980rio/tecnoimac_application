﻿using MagazineApp.DataGrid.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace MagazineApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticoloPage : ContentPage
    {
        public ArticoloPage()
        {
            InitializeComponent();
          
            #region CentralMessage
            
            MessagingCenter.Subscribe<string, string>("MyApp", "NotifyMsg", (sender, arg) =>
            {
                ED_CODE.Text = arg.ToString();
            });
            #endregion
            
            foreach (string colorName in nameToColor.Keys)
            {
               
                pickerfrom.Items.Add(colorName);
                pickerto.Items.Add(colorName);
            }
            
        }

        public  void ED_CODE_TextChanged(object sender, EventArgs args)
        {
        
        }

        public void BT_Cerca_Clicked(object sender, EventArgs args)
        {

        }

        #region PICKER
        // Dictionary to get Color from color name.
        Dictionary<string, int> nameToColor = new Dictionary<string, int>
        {
            { "1 ",    1  },
            { "2 ",    2  },
            { "3 ",    3  },
            { "4 ",    4  },
            { "5 ",    5  },
            { "6 ",    6  },
            { "7 ",    7  },
            { "8 ",    8  },
            { "9 ",    9  },
            { "10",    10 }
        };


        #endregion
    }
}