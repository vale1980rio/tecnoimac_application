﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace MagazineApp.Utility
{
    public class CheckLanStatus
    {
        private static bool CheckPort(string hostUri, int portNumber)
        {
            TcpClient tcpClient = new TcpClient();
            try
            {
                tcpClient.Connect(hostUri, portNumber);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CheckHost(string hostUri, int portNumber)
        {
            try
            {
                Ping ping = new Ping();
                PingReply pingReply = ping.Send(hostUri,1500);

                if (pingReply.Status == IPStatus.Success)
                {
                    return CheckPort(hostUri, portNumber);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool CheckOnlyHost(string hostUri)
        {
            try
            {
                Ping ping = new Ping();
                PingReply pingReply = ping.Send(hostUri, 1500);

                if (pingReply.Status == IPStatus.Success)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
