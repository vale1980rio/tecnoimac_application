﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using DevExpress.Mobile.DataGrid;
using DevExpress.Mobile.DataGrid.Theme;
using System.Globalization;
using DevExpress.Mobile.DataGrid.Localization;
using MagazineApp.DataGrid.Repository;
using MagazineApp.DataGrid.Objects;
using Syncfusion.SfNumericUpDown.XForms;
using Syncfusion.SfNumericTextBox.XForms;
using System.Reflection;
using System.Collections;
using System.Linq;

namespace MagazineApp.Utility
{


    class EditFormLocalizer_Movimentazioni : GridLocalizer
    {
        public void SetEditItemFormCaption()
        {
            AddString(GridStringId.EditingForm_LabelCaption, "MODIFICA QUANTITA'");
            AddString(GridStringId.DialogForm_ButtonCancel, "CANCELLA");
            AddString(GridStringId.DialogForm_ButtonOk, "SALVA");
        }
    }
    
    public class CustomEditFormContent_Movimentazioni : ContentView
    {
       
        public DevExpressArticoloMovimentazioniRepository ViewModel { get; set; }

        public CustomEditFormContent_Movimentazioni()
        {
            HorizontalOptions = LayoutOptions.FillAndExpand;
            VerticalOptions = LayoutOptions.FillAndExpand;
            GridLocalizer.Active = new EditFormLocalizer_Movimentazioni();
        }

       

       

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            EditFormLocalizer_Movimentazioni localizer = GridLocalizer.Active as EditFormLocalizer_Movimentazioni;

            if (localizer != null)
            {
                localizer.SetEditItemFormCaption();
            }

            Content = CreateEditItemFormContent();

          
            
        }

        SfNumericTextBox numeric;

        View CreateEditItemFormContent()
        {
            Grid layoutGrid = CreateGrid(5);
            FillFormGrid(layoutGrid, false);
            return layoutGrid;
        }
        

        Grid CreateGrid(int rowsCount)
        {
            Grid layoutGrid = new Grid();

            layoutGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            layoutGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(200, GridUnitType.Star) });

            for (int i = 0; i < rowsCount; i++)
            {
                layoutGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            }

            layoutGrid.RowSpacing = 20;
            layoutGrid.ColumnSpacing = 20;

            return layoutGrid;
        }

        void FillFormGrid(Grid layoutGrid, bool isNewItemForm)
        {
            FillLabels(layoutGrid, isNewItemForm);
            FillEditors(layoutGrid, isNewItemForm);
            
        }

        void FillLabels(Grid layoutGrid, bool isNewItemForm)
        {
            Label productName = new Label()
            {
                Text = "ARTICOLO:",
                FontSize = 14,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.DarkRed,
                VerticalOptions = LayoutOptions.Center
            };

            Label quantity = new Label()
            {
                Text = "QUANTITA':",
                FontSize = 14,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.DarkRed,
                VerticalOptions = LayoutOptions.Center
            };

            Label quantity_product = new Label()
            {
                Text = "QUANTITA' CONFEZIONE:",
                FontSize = 14,
                FontAttributes = FontAttributes.Bold,
                TextColor = Color.DarkRed,
                VerticalOptions = LayoutOptions.Center
            };

            layoutGrid.Children.Add(productName, 0, 0);
            layoutGrid.Children.Add(quantity, 0, 2);
            layoutGrid.Children.Add(quantity_product, 0, 3);

        }
       
        void FillEditors(Grid layoutGrid, bool isNewItemForm)
        {
            View productName;
            productName = new Label()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold,
                FontSize = 14

            };
            productName.SetBinding(Label.TextProperty, new Binding(EditValuesContainer.GetBindingPath("DESCRIZIONE")));


            StackLayout quantityStack = new StackLayout()
            {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };


          
            numeric = new SfNumericTextBox();
            numeric.TextAlignment = TextAlignment.Center;
            numeric.MaximumNumberDecimalDigits = 0;
    
            numeric.ValueChangeMode = Syncfusion.SfNumericTextBox.XForms.ValueChangeMode.OnKeyFocus;
            numeric.SetBinding(SfNumericTextBox.ValueProperty, EditValuesContainer.GetBindingPath("QUANTITA"), BindingMode.TwoWay, new Int32ToDoubleConverter());
            numeric.SelectAllOnFocus = true;
            numeric.AllowNull = false;
            
            quantityStack.Children.Add(numeric);


            View quantity_conf;
            quantity_conf = new Label()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center,
                FontAttributes = FontAttributes.Bold,
                FontSize = 14

            };
            quantity_conf.SetBinding(Label.TextProperty, new Binding(EditValuesContainer.GetBindingPath("QTACONF")));



            // Add created elements to a form.
            layoutGrid.Children.Add(productName, 1, 0);
            layoutGrid.Children.Add(quantityStack, 1, 2);
            layoutGrid.Children.Add(quantity_conf, 1, 3);

        }

        
    }

}