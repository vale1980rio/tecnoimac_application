﻿using MagazineApp.Database.SQLite.Objects;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;



namespace MagazineApp.Utility
{
    public class ArticoloPickerModel : INotifyPropertyChanged
    {

        List<Articolo_SQLite> articoli;
        public List<Articolo_SQLite> Articoli
        {
            get { return articoli; }
            set
            {
                if (articoli != value)
                {
                    articoli = value;
                    OnPropertyChanged();
                }
            }
        }

        Articolo_SQLite articolo;
        public Articolo_SQLite Articolo
        {
            get { return articolo; }
            set
            {
                if (articolo != value)
                {
                    articolo = value;
                    OnPropertyChanged();
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}