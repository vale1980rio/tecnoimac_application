﻿using DevExpress.Mobile.DataGrid;
using DevExpress.Mobile.DataGrid.Theme;
using MagazineApp.Database.MySQL;
using MagazineApp.Database.SQLite;
using MagazineApp.Database.SQLite.Objects;
using MagazineApp.DataGrid.Objects;
using MagazineApp.DataGrid.Repository;
using MagazineApp.Interfaces;
using MagazineApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MagazineApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VenditaManagerPage : CarouselPage
    {

       



        public VenditaManagerPage()
        {
            InitializeComponent();
            Carica_Dati_Griglia();
            Paint_Grid();
        }

      
        public void BT_Elimina_Click(object sender, EventArgs args)
        {

            try
            {
                Vendita_DEVEXPRESS obj = (Vendita_DEVEXPRESS)grid_vendite.SelectedDataObject;

                if (obj == null)
                {
                    DisplayAlert("VENDITA", "Nessuna Vendita Selezionata.", "OK");
                    return;
                }

                #region VENDITE

                string message = "NESSUN ERRORE";
                bool ok = false;




                SQLite_Manager slm = new SQLite_Manager();
                slm.DELETE_VENDITA_SQLite(obj.ID.ToString(), ref message, ref ok);


                #endregion

                string message_2 = "NESSUN ERRORE";
                bool ok_2 = false;

                #region ARTICOLI
                slm.DELETE_ARTICOLO__BY_FK_ARTICOLO_SQLite(obj.ID.ToString(), ref message_2, ref ok_2);

                if ((ok) && (ok_2))
                {
                    DevExpressVenditeRepository model_articoli = new DevExpressVenditeRepository();
                    grid_vendite.BindingContext = model_articoli;
                }
                else
                {
                    DisplayAlert("VENDITA", "ERRORE CANCELLAZIONE ARTICOLI: " + message_2 + System.Environment.NewLine + "ERRORE CANCELLAZIONE VENDITE: " + message, "OK");
                }
                #endregion
            }
            catch (Exception e)
            {
                DisplayAlert("VENDITA", e.Message, "OK");

            }
        }



        void Paint_Grid()
        {
            ThemeManager.ThemeName = Themes.Light;
            // Header customization.
            ThemeManager.Theme.HeaderCustomizer.BackgroundColor = Color.FromRgb(63, 112, 140);
            ThemeFontAttributes myFont = new ThemeFontAttributes("", ThemeFontAttributes.FontSizeFromNamedSize(NamedSize.Small), FontAttributes.Bold, Color.White);
            ThemeManager.Theme.HeaderCustomizer.Font = myFont;

            SQLite_Manager slm = new SQLite_Manager();
            string message = string.Empty;
            bool ok = false;


            //List<Magazzino_SQLite> magazzini = slm.GET_ALL_MAGAZZINI_SQLite(ref message, ref ok);


            //MagazzinoModel model = new MagazzinoModel();

            //model.Magazzini = new List<Magazzino_SQLite>();

            //foreach (var t in magazzini)
            //{
            //    model.Magazzini.Add(t);

            //}

            //pickermagazzini.BindingContext = model;
            //pickermagazzini.SelectedIndex = 0;

        }



        private void Carica_Dati_Griglia()
        {
            DevExpressClientiRepository model_clienti = new DevExpressClientiRepository();
            grid_clienti.BindingContext = model_clienti;

            DevExpressVenditeRepository model_vendite = new DevExpressVenditeRepository();
            grid_vendite.BindingContext = model_vendite;

            DevExpressMagazziniRepository model_magazzini = new DevExpressMagazziniRepository();
            grid_magazzini.BindingContext = model_magazzini;


        }



        public async void BT_VenditaPage_Click(object sender, EventArgs args)
        {
            Vendita_DEVEXPRESS obj = (Vendita_DEVEXPRESS)grid_vendite.SelectedDataObject;

            if (obj == null)
            {
                await DisplayAlert("VENDITA", "Nessuna Vendita Selezionata.", "OK");
                return;
            }
            await Navigation.PushAsync(new VenditaPage(obj));
        }

        public void BT_Save_Vendita_Click(object sender, EventArgs args)
        {

            string message_ = string.Empty;
            bool ok_ = false;
            SQLite_Manager slm = new SQLite_Manager();

            MySQL_Settings_SQLite settings = slm.GET_MySQL_SETTINGS(ref message_, ref ok_);


            bool ok_FTP = CheckLanStatus.CheckHost(settings.SERVER, 3306);

            if (!ok_FTP)
            {
                DisplayAlert("ERRORE", "Server Non Raggiungibile.", "OK");
                return;
            }



            //Prendo un ID VENDITA DAL SERVER MYSQL
            MySQL_Manager msm = new MySQL_Manager();

            Cliente_DEVEXPRESS obj = (Cliente_DEVEXPRESS)grid_clienti.SelectedDataObject;
            Vendita_DEVEXPRESS vendita = new Vendita_DEVEXPRESS();

            Magazzino_DEVEXPRESS magazzino = (Magazzino_DEVEXPRESS)grid_magazzini.SelectedDataObject;


            string message = "";
            bool ok = false;

            string ID_VENDITA = msm.GET_ID_VENDITA_MYSQL(obj.ID.ToString(), "", ref message, ref ok);


            //Se ho ottenuto l'id di vendita dal server MySQL continuo ok = true
            if (ok)
            {

                if (magazzino != null)
                {
                    vendita = slm.INSERT_VENDITA_SQLite(obj.ID.ToString(), "", ID_VENDITA, obj.CLIENTE.ToString(), obj.INDIRIZZO.ToString(), magazzino.CODICE, ref message, ref ok);
                    if (ok)
                    {
                        DevExpressVenditeRepository model_vendite = new DevExpressVenditeRepository();
                        grid_vendite.BindingContext = model_vendite;

                        Navigation.PushAsync(new VenditaPage(vendita));
                    }
                    else
                    {
                        DependencyService.Get<IMessage>().LongAlert(message);
                    }
                }
                else
                {
                    DisplayAlert("VENDITA", "Seleziona Un Magazzino.", "OK");
                }
            }
            else
            {
                DependencyService.Get<IMessage>().LongAlert(message);
            }

        }


    }
}
