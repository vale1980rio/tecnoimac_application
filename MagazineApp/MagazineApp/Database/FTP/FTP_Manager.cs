﻿
using MagazineApp.Database.SQLite;
using MagazineApp.Database.SQLite.Objects;
using MagazineApp.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace MagazineApp.Database.FTP
{
    public class FTP_Manager
    {
        public static List<Articolo_SQLite> GET_ALL_ARTICOLI_FTP(ref string message, ref bool ok)
        {
            if (File.Exists("/sdcard/ARTICOLI.TXT"))
            {
                File.Delete("/sdcard/ARTICOLI.TXT");
            }
            List<Articolo_SQLite> list = new List<Articolo_SQLite>();

            try
            {

                message = string.Empty;
                ok = false;
                SQLite_Manager slm = new SQLite_Manager();
                MySQL_Settings_SQLite settings = new MySQL_Settings_SQLite();
                settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);


                #region FTP
                if (ok)
                {
                    bool ok_2 = false;
                    message = "Nessun Errore";
                    DependencyService.Get<IFtpWebRequest>().download(ref ok_2, "FTP://" + settings.FTP_SERVER + "/DATA/ARTICOLI.TXT", "/sdcard/ARTICOLI.TXT");


                    if (ok_2)
                    {

                        System.IO.StreamReader file = new System.IO.StreamReader(@"/sdcard/ARTICOLI.TXT");
                        string line;

                        int count = 0;
                        while ((line = file.ReadLine()) != null)
                        {
                            try
                            {

                                string[] data = line.Split(';');


                                Articolo_SQLite articolo = new Articolo_SQLite();
                                articolo.ID = count.ToString();
                                articolo.EAN_128 = data[0];
                                articolo.DESCRIZIONE = data[1];
                                articolo.PZ_CONF = data[2];
                                articolo.EAN_13 = data[3];
                                articolo.COD_FORNITORE = data[4];
                                articolo.DESCRIZIONE_FORNITORE = data[5];
                                articolo.EAN_FORNITORE = data[6];
                                articolo.CODICE_ARTICOLO = data[7];
                                articolo.FLAG_X_CARICO = data[8];
                                articolo.PESO_UNITARIO = data[9];
                                articolo.SVILUPPO_ARTICOLO = data[10];
                                articolo.MAGAZZINO = data[11];
                                articolo.COD_BRICO = data[12];
                                articolo.SETTORE = data[13];
                                articolo.FILA = data[14];
                                articolo.PZ_X_MQ = data[15];
                                articolo.LASTUPDATE = DateTime.Now.ToString();
                                list.Add(articolo);

                                count++;


                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }

                    else
                    {
                        ok = false;
                        return null;
                    }

                }
                #endregion

                ok = true;
                return list;


            }
            catch (Exception ext)
            {
                message = ext.Message;
                ok = false;
                return null;
            }
        }

        public static List<Cliente_SQLite> GET_ALL_CLIENTI_FTP(ref string message, ref bool ok)
        {
            if (File.Exists("/sdcard/CLIENTI.TXT"))
            {
                File.Delete("/sdcard/CLIENTI.TXT");
            }
            List<Cliente_SQLite> list = new List<Cliente_SQLite>();
            try
            {

                message = string.Empty;
                ok = false;
                SQLite_Manager slm = new SQLite_Manager();
                MySQL_Settings_SQLite settings = new MySQL_Settings_SQLite();
                settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);

                if (ok)
                {
                    bool ok_2 = false;
                    message = "Nessun Errore";
                    DependencyService.Get<IFtpWebRequest>().download(ref ok_2, "FTP://" + settings.FTP_SERVER + "/DATA/CLIENTI.TXT", "/sdcard/CLIENTI.TXT");



                    if (ok_2)
                    {

                        System.IO.StreamReader file = new System.IO.StreamReader(@"/sdcard/CLIENTI.TXT");
                        string line;

                        while ((line = file.ReadLine()) != null)
                        {
                            string[] data = line.Split('\t');
                            Cliente_SQLite cliente = new Cliente_SQLite();

                            cliente.ID = Int32.Parse(data[0]);
                            cliente.CLIENTE = data[1];
                            cliente.INDIRIZZO = data[2];
                            cliente.COMUNE = data[3];
                            cliente.PROVINCIA = data[5];
                            cliente.CAP = data[4];
                            cliente.IVACF = data[6];
                            list.Add(cliente);
                        }
                    }
                    else
                    {
                        ok = false;
                        return null;
                    }

                }
                ok = true;
                return list;
            }
            catch (Exception ext)
            {
                message = ext.Message;
                ok = false;
                return null;
            }
        }

        public static List<Magazzino_SQLite> GET_ALL_MAGAZZINI_FTP(ref string message, ref bool ok)
        {

            if (File.Exists("/sdcard/MAGAZZINI.TXT"))
            {
                File.Delete("/sdcard/MAGAZZINI.TXT");
            }

            List<Magazzino_SQLite> list;
            Magazzino_SQLite trentacinque = new Magazzino_SQLite();
            list = new List<Magazzino_SQLite>();
            try
            {

                message = string.Empty;
                ok = false;
                SQLite_Manager slm = new SQLite_Manager();
                MySQL_Settings_SQLite settings = new MySQL_Settings_SQLite();
                settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);

                if (ok)
                {
                    bool ok_2 = false;
                    message = "Nessun Errore";
                    DependencyService.Get<IFtpWebRequest>().download(ref ok_2, "FTP://" + settings.FTP_SERVER + "/DATA/MAGAZZINI.TXT", "/sdcard/MAGAZZINI.TXT");

                    if (ok_2)
                    {


                        System.IO.StreamReader file = new System.IO.StreamReader(@"/sdcard/MAGAZZINI.TXT");
                        string line;

                        int id = 0;
                        while ((line = file.ReadLine()) != null)
                        {

                            string[] data = line.Split('\t');

                            if (data.Length == 2)
                            {
                                if (data[0].Trim() != "CODICE_TAB")
                                {
                                    Magazzino_SQLite magazzino = new Magazzino_SQLite();
                                    magazzino.IDMAGAZZINO = id;
                                    magazzino.CODICE = data[0];
                                    magazzino.DESCRIZIONE = data[1];
                                    list.Add(magazzino);
                                }
                            }
                        }

                    }
                    else
                    {
                        ok = false;
                        return null;
                    }
                }
                ok = true;


                return list.OrderBy(x => x.CODICE).ToList();



            }
            catch (Exception ext)
            {
                message = ext.Message;
                ok = false;
                return null;
            }
        }


    }
}
