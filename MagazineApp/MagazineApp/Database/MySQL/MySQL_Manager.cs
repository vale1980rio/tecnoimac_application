﻿using System;
using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using Xamarin.Forms;
using MagazineApp.Database.SQLite;
using MagazineApp.Database.SQLite.Objects;
using MagazineApp.Interfaces;

namespace MagazineApp.Database.MySQL
{
    public class MySQL_Manager
    {
        public string conn()
        {
            bool ok = false;
            string message = string.Empty;

            string server = string.Empty;
            string port = string.Empty;
            string database = string.Empty;
            string userid = string.Empty;
            string password = string.Empty;

            SQLite_Manager slm = new SQLite_Manager();
            MySQL_Settings_SQLite mySQl_Settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);


            if (ok)
            {
                server = mySQl_Settings.SERVER;
                port = mySQl_Settings.PORT.ToString();
                database = mySQl_Settings.DATABASE;
                userid = mySQl_Settings.USERID;
                password = mySQl_Settings.PASSWORD;
            }
            else { DependencyService.Get<IMessage>().ShortAlert(message); }

            return string.Format("Server={0};Port={1};database={2};User Id={3};Password={4};charset=utf8", server, port, database, userid, password);
        }
        
        public string GET_ID_VENDITA_MYSQL(string FK_Cliente, string Descrizione, ref string message, ref bool ok)
        {
            try
            {
                DataSet ds = new DataSet();
                MySqlConnection con = new MySqlConnection(conn());

                MySqlCommand cmd = new MySqlCommand("sp_ID_NUOVA_VENDITA", con);
                cmd.Parameters.AddWithValue("FK_Cliente", FK_Cliente);
                cmd.Parameters.AddWithValue("Descrizione", Descrizione);
                cmd.CommandType = CommandType.StoredProcedure;

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);

                message = "Caricamento Dati Da MySQL Effettuato con Successo (TABELLA VENDITA)";
                ok = true;
                return ds.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                message = "ERRORE: " + e.Message;
                ok = false;
                return null;
            }
        }

        public string GET_ID_MOVIMENTAZIONE_MYSQL(string FK_Magazzino_Partenza, string FK_Magazzino_Arrivo, ref string message, ref bool ok)
        {
            try
            {
                DataSet ds = new DataSet();
                MySqlConnection con = new MySqlConnection(conn());

                MySqlCommand cmd = new MySqlCommand("sp_ID_NUOVA_MOVIMENTAZIONE", con);
                cmd.Parameters.AddWithValue("FK_Magazzino_Partenza", FK_Magazzino_Partenza);
                cmd.Parameters.AddWithValue("FK_Magazzino_Arrivo", FK_Magazzino_Arrivo);

                cmd.CommandType = CommandType.StoredProcedure;

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                da.Fill(ds);

                message = "Caricamento Dati Da MySQL Effettuato con Successo (TABELLA MOVIMENTAZIONE)";
                ok = true;
                return ds.Tables[0].Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                message = "ERRORE: " + e.Message;
                ok = false;
                return null;
            }
        }
    }
}
