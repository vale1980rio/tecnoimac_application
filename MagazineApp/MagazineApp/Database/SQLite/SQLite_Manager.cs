﻿using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using MagazineApp.Interfaces;
using DevExpress.Mobile.Core.Containers;
using System;
using MagazineApp.Database.SQLite.Objects;
using AutoMapper;
using MagazineApp.Mappings;
using MagazineApp.DataGrid.Objects;

namespace MagazineApp.Database.SQLite
{
    public class SQLite_Manager
    {
        private SQLiteConnection database;

        //Costruttore che ad ogni invocazione crea una connessione al db locale
        public SQLite_Manager()
        {
            //Prendo la path dove sta il file db3
            database = DependencyService.Get<IDatabaseConnection>().DbConn();
            //Se non esiste creo la tabella Articolo
        }





        #region ARTICOLI
        public void ADD_ARTICOLI_FROM_SQLite(List<Articolo_SQLite> Articoli_SQLite, ref string message, ref bool ok, ref int count)
        {
            try
            {
                database.BeginTransaction();

                database.DropTable<Articolo_SQLite>();
                database.CreateTable<Articolo_SQLite>();

                int count_id = 0;
                foreach (var item in Articoli_SQLite)
                {
                    database.Insert(item);
                }
                message = "Dati Aggiornati.";
                count = database.Table<Articolo_SQLite>().Count();
                ok = true;
                database.Commit();
                
            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }
        }
        //Metodo per prendere sul SQLite ultima data caricamento
        public string GET_LAST_UPDATE_ADD_ARTICOLI(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {


                string data = database.Query<Articolo_SQLite>("SELECT * FROM Articoli LIMIT 1")[0].LASTUPDATE;
                DateTime date;

                if (DateTime.TryParse(data, out date))
                {
                    ok = true;
                    database.Commit();
                    
                    return string.Format("{0:dd/MM/yyyy HH:mm}", date);
                }
                else
                {
                    ok = true;
                    database.Rollback();
                    
                    return "Sistema Aggiornato, Ma Nessuna Data Disponibile.";
                }

            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


                return "";
            }

        }

        public List<Articolo_SQLite> GET_ARTICOLI_SQLite(string EAN, string MAGAZZINO, ref string message, ref bool ok, ref int count)
        {
            database.BeginTransaction();
            try
            {
                //Prendo Tutti Gli Articoli Con EAN INDICATO
                List<Articolo_SQLite> data = database.Table<Articolo_SQLite>().Where(p =>
                (
                    (
                        (p.EAN_128 == EAN) ||
                        (p.EAN_13 == EAN)) ||
                        (p.EAN_FORNITORE == EAN)
                    )
                ).ToList();
                ok = true;

                database.Commit();
                
                message = "Dati MySQL Presi.";

                count = data.Count();

                //PRENDO TUTTI GLI ARTICOLI INDICATI DAL MAGAZZINO INDICATO
                var test = data.Where(p => p.MAGAZZINO == MAGAZZINO);
                return test.ToList();
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }
        }


        public Articolo_SQLite GET_ARTICOLO_SQLite(string EAN, string MAGAZZINO, ref string message, ref bool ok, ref int count)
        {
            database.BeginTransaction();
            try
            {
                List<Articolo_SQLite> data = database.Table<Articolo_SQLite>().Where(p =>
                (
                    (
                        (p.EAN_128 == EAN) ||
                        (p.EAN_13 == EAN)) ||
                        (p.EAN_FORNITORE == EAN)
                    ) && p.MAGAZZINO == MAGAZZINO
                ).ToList();
                ok = true;



                database.Commit();
                
                message = "Dati MySQL Presi.";

                count = data.Count();

                var test = data.Where(p => p.MAGAZZINO == MAGAZZINO).First();
                return test;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }
        }

        public List<Articolo_SQLite> CHECK_ARTICOLO_MAGAZZINO_ARRIVO_SQLite(string DESCRIZIONE, string MAGAZZINOA, ref string message, ref bool ok, ref int count)
        {
            database.BeginTransaction();
            try
            {
                //prendo tutti gli articoli con EAN indicato
                List<Articolo_SQLite> data = database.Table<Articolo_SQLite>().Where(p => ((p.DESCRIZIONE == DESCRIZIONE) && (p.MAGAZZINO == MAGAZZINOA))).ToList();
                ok = true;

                database.Commit();
                
                message = "Dati MySQL Presi.";

                count = data.Count();

                return data;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
                return null;
            }
        }



        public List<Articolo_SQLite> GET_ARTICOLO_MOVIMENTAZIONE_SQLite(string EAN, string MAGAZZINOP, string MAGAZZINOA, ref string message, ref bool ok, ref int count)
        {
            try
            {
                database.BeginTransaction();

                //prendo tutti gli articoli con EAN indicato
                List<Articolo_SQLite> data = database.Table<Articolo_SQLite>().Where(p => (((p.EAN_128 == EAN) || (p.EAN_13 == EAN)) || (p.EAN_FORNITORE == EAN))).ToList();
                ok = true;

                database.Commit();
                

                message = "Dati MySQL Presi.";

                count = data.Count();

                //Controllo se Presente Magazzino Di Partenza 
                List<Articolo_SQLite> da_inviare = data.Where(p => p.MAGAZZINO == MAGAZZINOP).ToList();

                if (da_inviare != null)
                {
                    int count_elementi = da_inviare.Count();

                    // SE NON CE' L'ARTICOLO CERCATO
                    if (count_elementi == 0)
                    {
                        ok = false;
                        message = string.Format("Articolo Non Presente In Magazzino Di Partenza. ({0})", MAGAZZINOP);
                        return null;
                    }

                    //SE CI SONO PIU' ARTICOLI CON LO STESSO CODICE LI SPARO SOPRA
                    else if (count_elementi > 1)
                    {
                        return da_inviare;
                    }

                    //SE TROVO UN SOLO ARTICOLO CONTROLLO SE E' PRESENTE NEL MAGAZZINO DI ARRIVO
                    else if (count_elementi == 1)
                    {
                        #region UNO

                        //Allora Controllo Se è presente nel magazzino di arrivo.
                        var presente_in_magazzino_arrivo = data.Where(p => p.MAGAZZINO == MAGAZZINOA);

                        if (presente_in_magazzino_arrivo != null)
                        {
                            if (presente_in_magazzino_arrivo.Count() == 0)
                            {
                                ok = false;
                                message = string.Format("Articolo Non Presente In Magazzino Di Arrivo. ({0})", MAGAZZINOA);
                                return null;
                            }
                            else if (presente_in_magazzino_arrivo.Count() == 1)
                            {
                                //UNICO VALORE AMMESSO!!!
                                return da_inviare;
                            }
                            else if (presente_in_magazzino_arrivo.Count() > 1)
                            {
                                ok = false;
                                message = string.Format("Articolo DUPLICATO In Magazzino Di Arrivo. ({0})", MAGAZZINOA);
                                return null;
                            }
                            else
                            {
                                ok = false;
                                message = string.Format("Articolo Non Presente In Magazzino Di Arrivo. ({0})", MAGAZZINOA);
                                return null;
                            }
                        }
                        else
                        {
                            ok = false;
                            message = string.Format("Articolo Non Presente In Magazzino Di Arrivo. ({0})", MAGAZZINOA);
                            return null;
                        }
                        #endregion
                    }
                }
                else
                {
                    ok = false;
                    message = string.Format("Articolo Non Presente In Magazzino Di Partenza. ({0})", MAGAZZINOP);
                    return null;
                }
            }
            catch (Exception e)
            {
                ok = false;
                message = e.Message;

                database.Rollback();
                
            }

            ok = false;
            return null;

        }


        public void DELETE_ARTICOLO_SQLite(string code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.Query<Articolo_Vendita_SQLite>("DELETE FROM ArticoliVendita where ID = " + code);

                database.Commit();
                

                message = "Dati MySQL Cancellati.";
                ok = true;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }
        }

        public void DELETE_ARTICOLO__BY_FK_ARTICOLO_SQLite(string code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.Query<Articolo_Vendita_SQLite>("DELETE FROM ArticoliVendita where FK_VENDITA = " + code);

                database.Commit();
                
                message = "Dati MySQL Cancellati.";
                ok = true;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }
        }

        public BindingList<Articolo_DEVEXPRESS> GET_ARTICOLI_BYCODE(string Code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {

                List<Articolo_SQLite> data = database.Query<Articolo_SQLite>("SELECT Articoli.*,  ArticoliVendita.Quantita as QUANTITA, ArticoliVendita.APPROVATO, ArticoliVendita.ID as FK_ARTICOLO_VENDITA FROM ArticoliVendita inner join Articoli on Articoli.ID = ArticoliVendita.FK_ARTICOLO where ArticoliVendita.FK_Vendita = " + Code).ToList();
                MapperConfiguration configuration;
                configuration = new MapperConfiguration(a => { a.AddProfile(new Articolo_SQLite_Articolo_DEVEXPRESS()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var result = mapper.Map<List<Articolo_SQLite>, BindingList<Articolo_DEVEXPRESS>>(data.ToList());

                message = "Dati Caricati Correttamente.";
                ok = true;
                database.Commit();
                
                return result;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
                return null;
            }
        }

        public Articolo_DEVEXPRESS GET_ARTICOLI_BY_ID(string Code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {

                Articolo_SQLite data = database.Query<Articolo_SQLite>("SELECT Articoli.*,  ArticoliVendita.Quantita as QUANTITA, ArticoliVendita.APPROVATO, ArticoliVendita.ID as FK_ARTICOLO_VENDITA FROM ArticoliVendita inner join Articoli on Articoli.ID = ArticoliVendita.FK_ARTICOLO where ArticoliVendita.ID = " + Code).First();
                MapperConfiguration configuration;
                configuration = new MapperConfiguration(a => { a.AddProfile(new Articolo_SQLite_Articolo_DEVEXPRESS()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var result = mapper.Map<Articolo_SQLite, Articolo_DEVEXPRESS>(data);

                message = "Dati Caricati Correttamente.";
                ok = true;
                database.Commit();
                
                return result;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
                return null;
            }
        }

        #endregion

        #region MySQL SETTINGS

        public void ADD_MySQL_SETTINGS(MySQL_Settings_SQLite mySQL_Settings_SQLite, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            database.CreateTable<MySQL_Settings_SQLite>();

            try
            {
                if (database.Table<MySQL_Settings_SQLite>().Count() == 1)
                {
                    var query = "UPDATE Settings " +
                           "SET SERVER = '" + mySQL_Settings_SQLite.SERVER + "'," +
                           " PORT = " + mySQL_Settings_SQLite.PORT + "," +
                           " DATABASE = '" + mySQL_Settings_SQLite.DATABASE + "'," +
                           " USERID = '" + mySQL_Settings_SQLite.USERID + "'," +
                           " PASSWORD = '" + mySQL_Settings_SQLite.PASSWORD + "'";
                    database.Query<MySQL_Settings_SQLite>(query);

                    ok = true;
                    message = "DatiAggiornati";
                }
                else if (database.Table<MySQL_Settings_SQLite>().Count() == 0)
                {

                    database.CreateTable<MySQL_Settings_SQLite>();
                    database.Insert(mySQL_Settings_SQLite);
                    message = "Dati MySQL Caricati.";
                    ok = true;
                }
                database.Commit();
                
            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }
        }

        public MySQL_Settings_SQLite GET_MySQL_SETTINGS(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            database.CreateTable<MySQL_Settings_SQLite>();

            try
            {
                List<MySQL_Settings_SQLite> data = database.Table<MySQL_Settings_SQLite>().ToList();
                ok = true;

                database.Commit();
                

                message = "Dati MySQL Aggiornati.";

                if (database.Table<MySQL_Settings_SQLite>().Count() > 0)
                {
                    return data.First();
                }
                else
                {
                    MySQL_Settings_SQLite MySQL_Settings_SQLite = new MySQL_Settings_SQLite();
                    return MySQL_Settings_SQLite;
                }
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }


        }

        public void UPDATE_DEVICE_SETTINGS_SQLite(string DEVICE_NAME, string PASSWORD, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            database.CreateTable<MySQL_Settings_SQLite>();

            try
            {
                if (database.Table<MySQL_Settings_SQLite>().Count() > 0)
                {
                    database.Query<MySQL_Settings_SQLite>("UPDATE Settings SET DEVICE_NAME = '" + DEVICE_NAME + "', DEVICE_PASSWORD='" + PASSWORD + "'");

                    ok = true;
                    message = "DatiAggiornati";
                }

                else if (database.Table<MySQL_Settings_SQLite>().Count() == 0)
                {
                    MySQL_Settings_SQLite mySQL_Settings_SQLite = new MySQL_Settings_SQLite();
                    mySQL_Settings_SQLite.DEVICE_NAME = DEVICE_NAME;
                    mySQL_Settings_SQLite.DEVICE_PASSWORD = PASSWORD;

                    database.BeginTransaction();

                    database.DropTable<MySQL_Settings_SQLite>();
                    database.CreateTable<MySQL_Settings_SQLite>();
                    database.Insert(mySQL_Settings_SQLite);

                    message = "DatiAggiornati";
                    ok = true;

                }

                database.Commit();
                

            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }


        }

        public void UPDATE_FTP_SETTINGS_SQLite(string FTP_SERVER, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            database.CreateTable<MySQL_Settings_SQLite>();

            try
            {
                if (database.Table<MySQL_Settings_SQLite>().Count() > 0)
                {
                    database.Query<MySQL_Settings_SQLite>("UPDATE Settings SET FTP_SERVER = '" + FTP_SERVER + "'");
                    ok = true;
                    message = "DatiAggiornati";
                }

                else if (database.Table<MySQL_Settings_SQLite>().Count() == 0)
                {
                    MySQL_Settings_SQLite mySQL_Settings_SQLite = new MySQL_Settings_SQLite();
                    mySQL_Settings_SQLite.FTP_SERVER = FTP_SERVER;

                    database.BeginTransaction();

                    database.DropTable<MySQL_Settings_SQLite>();
                    database.CreateTable<MySQL_Settings_SQLite>();
                    database.Insert(mySQL_Settings_SQLite);

                    message = "DatiAggiornati";
                    ok = true;
                }

                database.Commit();
                


            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }


        }

        public void DEFAULT_DEVICE_SETTINGS_SQLite(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            database.CreateTable<MySQL_Settings_SQLite>();

            try
            {
                if (database.Table<MySQL_Settings_SQLite>().Count() == 0)
                {
                    MySQL_Settings_SQLite mySQL_Settings_SQLite = new MySQL_Settings_SQLite();
                    mySQL_Settings_SQLite.DEVICE_NAME = "DEVICE";
                    mySQL_Settings_SQLite.DEVICE_PASSWORD = "admin";



                    database.DropTable<MySQL_Settings_SQLite>();
                    database.CreateTable<MySQL_Settings_SQLite>();
                    database.Insert(mySQL_Settings_SQLite);

                    message = "DatiAggiornati";
                    ok = true;

                }

                database.Commit();
                

            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }


        }
        #endregion

        #region STAMPANTE
        public void ADD_STAMPANTI_SQLite(Stampante_SQLite stampante_SQLite, ref string message, ref bool ok)
        {
            try
            {
                database.BeginTransaction();

                database.CreateTable<Stampante_SQLite>();
                database.Insert(stampante_SQLite);


                message = "Dati Stampante Caricati.";
                ok = true;
                database.Commit();
                
            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }


        }
        public void DELETE_STAMPANTE_SQLite(Stampante_SQLite stampante_SQLite, ref string message, ref bool ok)
        {
            try
            {
                database.BeginTransaction();
                database.Delete(stampante_SQLite);

                message = "Dati Stampante Rimossi.";
                ok = true;
                database.Commit();
                
            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }


        }
        public BindingList<Stampante_DEVEXPRESS> GET_ALL_STAMPANTE_SQLite(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Stampante_SQLite>();

                List<Stampante_SQLite> data = database.Table<Stampante_SQLite>().ToList();
                ok = true;

                database.Commit();
                

                message = "Dati Stampanti Caricati.";


                MapperConfiguration configuration;
                configuration = new MapperConfiguration(a => { a.AddProfile(new Stampante_SQLite__Stampante_DEVEXPRESS_Profile()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var result = mapper.Map<List<Stampante_SQLite>, BindingList<Stampante_DEVEXPRESS>>(data.ToList());

                return result;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }


        }
        #endregion

        #region CLIENTI
        public BindingList<Cliente_DEVEXPRESS> GET_ALL_CLIENTI_SQLite(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {

                database.CreateTable<Cliente_SQLite>();

                List<Cliente_SQLite> data = database.Table<Cliente_SQLite>().ToList();
                ok = true;

                database.Commit();
                

                message = "Dati Stampanti Caricati.";

                MapperConfiguration configuration;
                configuration = new MapperConfiguration(a => { a.AddProfile(new Cliente_SQLite__Cliente_DEVEXPRESS_Profile()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var result = mapper.Map<List<Cliente_SQLite>, BindingList<Cliente_DEVEXPRESS>>(data.ToList());

                return result;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }


        }

        public Cliente_SQLite GET_CLIENTE_SQLite(string id, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                Cliente_SQLite cliente_SQLite = new Cliente_SQLite();
                cliente_SQLite = database.Query<Cliente_SQLite>("SELECT * from Clienti WHERE ID = " + id).First();



                ok = true;

                database.Commit();
                
                message = "Dati Stampanti Caricati.";



                return cliente_SQLite;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }


        }

        public void ADD_CLIENTI_SQLite(List<Cliente_SQLite> Clienti_SQLite, ref string message, ref bool ok)
        {
            try
            {
                database.BeginTransaction();
                database.DropTable<Cliente_SQLite>();
                database.CreateTable<Cliente_SQLite>();

                foreach (var item in Clienti_SQLite)
                {

                    database.Insert(item);
                }

                message = "Dati Cliente Caricati.";
                ok = true;
                database.Commit();
                
            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }


        }
        #endregion

        #region VENDITA
        public int VENDITE_COUNT()
        {
            try
            {
                database.BeginTransaction();
                List<Vendita_SQLite> data = database.Table<Vendita_SQLite>().ToList();
                database.Commit();
                
                return data.Count();
            }
            catch (Exception e)
            {
                database.Rollback();
                
                return 0;
            }
        }

        public Vendita_DEVEXPRESS INSERT_VENDITA_SQLite(string FK_Cliente, string Descrizione_Vendita, string ID_MySQL, string Cliente, string Cliente_Indirizzo, string Magazzino, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Vendita_SQLite>();
                Vendita_SQLite vendita_SQLite = new Vendita_SQLite();
                vendita_SQLite.FK_CLIENTE = Int32.Parse(FK_Cliente);
                vendita_SQLite.DESCRIZIONE_VENDITA = Descrizione_Vendita;
                vendita_SQLite.ID = Int32.Parse(ID_MySQL);
                vendita_SQLite.INDIRIZZO_CLIENTE = Cliente_Indirizzo;
                vendita_SQLite.CLIENTE = Cliente;
                vendita_SQLite.MAGAZZINO = Magazzino;

                vendita_SQLite.CREAZIONE = DateTime.Now;
                database.Insert(vendita_SQLite);
                database.Commit();
                

                ok = true;

                //fare un mapping
                MapperConfiguration configuration = new MapperConfiguration(a => { a.AddProfile(new Vendita_SQLite__Vendita_DEVEXPRESS_Profile()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                Vendita_DEVEXPRESS vendita_DEVEXPRESS = mapper.Map<Vendita_SQLite, Vendita_DEVEXPRESS>(vendita_SQLite);

                message = "Dati  Caricati.";
                return vendita_DEVEXPRESS;

            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
                return null;
            }


        }

        public int INSERT_ARTICOLO_VENDITA_SQLite(string FK_Articolo, string FK_Vendita, int quantita, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Articolo_Vendita_SQLite>();
                Articolo_Vendita_SQLite articolo_Vendita_SQLite = new Articolo_Vendita_SQLite();
                articolo_Vendita_SQLite.FK_ARTICOLO = FK_Articolo;
                articolo_Vendita_SQLite.FK_VENDITA = FK_Vendita;
                articolo_Vendita_SQLite.QUANTITA = quantita;
                articolo_Vendita_SQLite.APPROVATO = false;
                database.Insert(articolo_Vendita_SQLite);
                database.Commit();
                

                ok = true;
                int fk = -1;
                try
                {
                    fk = database.Table<Articolo_Vendita_SQLite>().Last().ID;
                }
                catch
                {
                    fk = -1;
                }

                message = "Dati  Caricati.";

                return fk;


            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
                return -1;
            }


        }

        public void INSERT_ARTICOLO_MOVIMENTAZIONE_SQLite(string FK_Articolo, string FK_Movimentazione, int quantita, ref string message, ref bool ok)

        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Articolo_Movimentazione_SQLite>();
                Articolo_Movimentazione_SQLite articolo_Vendita_SQLite = new Articolo_Movimentazione_SQLite();
                articolo_Vendita_SQLite.FK_ARTICOLO = FK_Articolo;
                articolo_Vendita_SQLite.FK_MOVIMENTAZIONE = FK_Movimentazione;
                articolo_Vendita_SQLite.QUANTITA = quantita;
                articolo_Vendita_SQLite.APPROVATO = false;
                database.Insert(articolo_Vendita_SQLite);
                database.Commit();
                
                ok = true;


                message = "Dati  Caricati.";



            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }


        }

        public BindingList<Vendita_DEVEXPRESS> GET_ALL_VENDITE_SQLite(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Vendita_SQLite>();

                List<Vendita_SQLite> data = database.Table<Vendita_SQLite>().ToList();
                ok = true;

                database.Commit();
                

                message = "Dati Vendite Caricati.";


                MapperConfiguration configuration;
                configuration = new MapperConfiguration(a => { a.AddProfile(new Vendita_SQLite__Vendita_DEVEXPRESS_Profile()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var result = mapper.Map<List<Vendita_SQLite>, BindingList<Vendita_DEVEXPRESS>>(data.ToList());

                return result;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }


        }

        public void UPDATE_ALL_VENDITE_SQLite(BindingList<Articolo_DEVEXPRESS> articoli, ref string message, ref bool ok)
        {

            foreach (var t in articoli)
            {

                string pezzi = t.PZ_CONF.Trim().Replace(",00", "");
                if (pezzi == t.QUANTITA)
                {
                    t.APPROVATO = "0";
                }
                else
                {
                    t.APPROVATO = "1";
                }
            }

            database.BeginTransaction();
            try
            {
                foreach (var t in articoli)
                {
                    database.Query<Articolo_Vendita_SQLite>("UPDATE ArticoliVendita SET Quantita = " + t.QUANTITA + ", Approvato = '" + t.APPROVATO + "' WHERE ID = " + t.FK_ARTICOLO_VENDITA);
                }

                database.Commit();
                

                ok = true;
                message = "Dati Quantità Articoli";

            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }


        }
        public void UPDATE_ALL_MOVIMENTAZIONI_SQLite(BindingList<Articolo_DEVEXPRESS> articoli, ref string message, ref bool ok)
        {

            foreach (var t in articoli)
            {
                string pezzi = t.PZ_CONF.Trim().Replace(",00", "");
                if (pezzi == t.QUANTITA)
                {
                    t.APPROVATO = "0";
                }
                else
                {
                    t.APPROVATO = "1";
                }
            }

            database.BeginTransaction();
            try
            {
                foreach (var t in articoli)
                {
                    database.Query<Articolo_Movimentazione_SQLite>("UPDATE ArticoliMovimentazione SET Quantita = " + t.QUANTITA + ", Approvato = '" + t.APPROVATO + "' WHERE ID = " + t.FK_ARTICOLO_VENDITA);
                }

                database.Commit();
                
                ok = true;
                message = "Dati Quantità Articoli";

            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }


        }

        public void DELETE_VENDITA_SQLite(string code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.Query<Vendita_SQLite>("DELETE FROM Vendita where ID = " + code);
                database.Commit();
                

                message = "Dati MySQL Cancellati.";
                ok = true;

            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }
        }


        #endregion

        #region MAGAZZINI

        public List<Magazzino_DEVEXPRESS> GET_ALL_MAGAZZINI(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Magazzino_SQLite>();

                List<Magazzino_SQLite> data = database.Table<Magazzino_SQLite>().ToList();
                ok = true;

                database.Commit();
                

                message = "Dati Magazzino Caricati.";

                //fare un mapping
                MapperConfiguration configuration = new MapperConfiguration(a => { a.AddProfile(new Magazzino_SQLite__Magazzino_DEVEXPRESS_Profile()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var Magazzini_DEVEXPRESS = mapper.Map<List<Magazzino_SQLite>, List<Magazzino_DEVEXPRESS>>(data);


                return Magazzini_DEVEXPRESS;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }












        }

        public void ADD_MAGAZZINI_SQLite(List<Magazzino_SQLite> Magazzini_SQLite, ref string message, ref bool ok)
        {
            try
            {
                database.BeginTransaction();
                database.DropTable<Magazzino_SQLite>();
                database.CreateTable<Magazzino_SQLite>();


                foreach (var item in Magazzini_SQLite)
                {
                    database.Insert(item);
                }

                message = "Dati Magazzino Caricati.";
                ok = true;
                database.Commit();
                
            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }


        }
        #endregion

        #region MOVIMENTAZIONI 

        public int MOVIMENTAZIONI_COUNT()
        {
            try
            {
                database.BeginTransaction();
                List<Movimentazione_SQLite> data = database.Table<Movimentazione_SQLite>().ToList();
                database.Commit();
                

                return data.Count();

            }
            catch (Exception e)
            {
                database.Rollback();
                
                return 0;
            }
        }
        public BindingList<Movimentazione_DEVEXPRESS> GET_ALL_MOVIMENTAZIONI(ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Movimentazione_SQLite>();

                List<Movimentazione_SQLite> data = database.Table<Movimentazione_SQLite>().ToList();
                ok = true;

                database.Commit();
                

                message = "Dati Magazzino Caricati.";

                //fare un mapping
                MapperConfiguration configuration = new MapperConfiguration(a => { a.AddProfile(new Magazzino_SQLite__Magazzino_DEVEXPRESS_Profile()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var Magazzini_DEVEXPRESS = mapper.Map<List<Movimentazione_SQLite>, BindingList<Movimentazione_DEVEXPRESS>>(data);


                return Magazzini_DEVEXPRESS;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }


        }
        public BindingList<Articolo_DEVEXPRESS> GET_ARTICOLIMOVIMENTAZIONI_BYCODE(string Code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                List<Articolo_SQLite> data = database.Query<Articolo_SQLite>("SELECT Articoli.*,  ArticoliMovimentazione.Quantita as QUANTITA, ArticoliMovimentazione.APPROVATO, ArticoliMovimentazione.ID as FK_ARTICOLO_VENDITA FROM ArticoliMovimentazione inner join Articoli on Articoli.ID = ArticoliMovimentazione.FK_ARTICOLO where ArticoliMovimentazione.FK_MOVIMENTAZIONE = " + Code).ToList();


                MapperConfiguration configuration;
                configuration = new MapperConfiguration(a => { a.AddProfile(new Articolo_SQLite_Articolo_DEVEXPRESS()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();

                var result = mapper.Map<List<Articolo_SQLite>, BindingList<Articolo_DEVEXPRESS>>(data.ToList());

                message = "Dati Caricati Correttamente.";
                ok = true;
                database.Commit();
                

                return result;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

                return null;
            }
        }
        public void INSERT_MAGAZZINI_MOVIMENTAZIONE(ref Movimentazione_SQLite Movimentazione_SQLite, string ID_MOVIMENTAZIONE, string FK_MAGAZZINO_PARTENZA, string CODICE_MAGAZZINO_PARTENZA, string DESCRIZIONE_MAGAZZINO_PARTENZA, string FK_MAGAZZINO_ARRIVO, string CODICE_MAGAZZINO_ARRIVO, string DESCRIZIONE_MAGAZZINO_ARRIVO, DateTime DATA_CREAZIONE, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.CreateTable<Movimentazione_SQLite>();
                Movimentazione_SQLite = new Movimentazione_SQLite();

                Movimentazione_SQLite.FK_MAGAZZINO_PARTENZA = FK_MAGAZZINO_PARTENZA;
                Movimentazione_SQLite.CODICE_MAGAZZINO_PARTENZA = CODICE_MAGAZZINO_PARTENZA;
                Movimentazione_SQLite.DESCRIZIONE_MAGAZZINO_PARTENZA = DESCRIZIONE_MAGAZZINO_PARTENZA;

                Movimentazione_SQLite.FK_MAGAZZINO_ARRIVO = FK_MAGAZZINO_ARRIVO;
                Movimentazione_SQLite.CODICE_MAGAZZINO_ARRIVO = CODICE_MAGAZZINO_ARRIVO;
                Movimentazione_SQLite.DESCRIZIONE_MAGAZZINO_ARRIVO = DESCRIZIONE_MAGAZZINO_ARRIVO;

                Movimentazione_SQLite.ID_MOVIMENTAZIONE = ID_MOVIMENTAZIONE;
                Movimentazione_SQLite.DATA_CREAZIONE = DATA_CREAZIONE;

                database.Insert(Movimentazione_SQLite);
                database.Commit();
                

                ok = true;

            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                

            }


        }
        public void DELETE_MOVIMENTAZIONE_SQLite(string code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.Query<Movimentazione_SQLite>("DELETE FROM Movimentazioni where ID = " + code);
                database.Commit();
                
                message = "Dati MySQL Cancellati.";
                ok = true;
            }
            catch (Exception ext)
            {
                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                
            }
        }


        public void DELETE_ARTICOLOMOVIMENTAZIONE__BY_FK_ARTICOLO_SQLite(string code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.Query<Articolo_Movimentazione_SQLite>("DELETE FROM ArticoliMovimentazione where FK_Movimentazione = " + code);

                database.Commit();
                
                message = "Dati MySQL Cancellati.";
                ok = true;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }
        }

        public void DELETE_ARTICOLO_MOVIMENTAZIONE_SQLite(string code, ref string message, ref bool ok)
        {
            database.BeginTransaction();
            try
            {
                database.Query<Articolo_Movimentazione_SQLite>("DELETE FROM ArticoliMovimentazione where ID = " + code);

                database.Commit();
                
                message = "Dati MySQL Cancellati.";
                ok = true;
            }
            catch (Exception ext)
            {

                message = "Errore: " + ext.Message;
                ok = false;
                database.Rollback();
                


            }
        }


        #endregion

        #region RESET
        public void RESET()
        {
            database.CreateTable<Articolo_Movimentazione_SQLite>();
            database.CreateTable<Articolo_Vendita_SQLite>();
            database.CreateTable<Movimentazione_SQLite>();
            database.CreateTable<Vendita_SQLite>();

            database.DeleteAll<Articolo_Movimentazione_SQLite>();
            database.DeleteAll<Articolo_Vendita_SQLite>();
            database.DeleteAll<Movimentazione_SQLite>();
            database.DeleteAll<Vendita_SQLite>();
        }
        #endregion
    }
}

