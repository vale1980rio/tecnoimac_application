﻿using SQLite;
using System; 
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("Settings")]
    public class MySQL_Settings_SQLite
    {
        [PrimaryKey,AutoIncrement]
        public int ID { get; set; }

        public string SERVER { get; set; }
        public string PORT { get; set; }
        public string DATABASE { get; set; }
        public string USERID { get; set; }
        public string PASSWORD { get; set; }
        public string DEVICE_NAME { get; set; }
        public string DEVICE_PASSWORD { get; set; }
        public string FTP_SERVER { get; set; }
       
        public bool MANUAL { get; set; }
    }
}
