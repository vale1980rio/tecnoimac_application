﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("Vendita")]
    public class Vendita_SQLite
    {
        [PrimaryKey][AutoIncrement]
        public int ID_TABLE { get; set; }
        public int ID { get; set; }
        public int FK_CLIENTE { get; set; }
        public string DESCRIZIONE { get; set; }
        public string DESCRIZIONE_VENDITA { get; set; }
        public string CLIENTE { get; set; }
        public string INDIRIZZO_CLIENTE { get; set; }
        public string MAGAZZINO { get; set; }

        public DateTime CREAZIONE { get; set; }
    }
}
