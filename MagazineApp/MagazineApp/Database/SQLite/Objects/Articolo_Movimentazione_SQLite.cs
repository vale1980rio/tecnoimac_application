﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("ArticoliMovimentazione")]
    public class Articolo_Movimentazione_SQLite
    {
        [PrimaryKey][AutoIncrement]
        public int ID { get; set; }
        public string FK_MOVIMENTAZIONE { get; set; }
        public string FK_ARTICOLO { get; set; }
        public int QUANTITA { get; set; }
        public bool APPROVATO { get; set; }
    }
}
