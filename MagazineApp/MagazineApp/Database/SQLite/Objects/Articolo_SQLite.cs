﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("Articoli")]
    public class Articolo_SQLite
    {
      
        public string ID { get; set; }
        public string EAN_128 { get; set; }
        public string DESCRIZIONE { get; set; }
        public string PZ_CONF { get; set; }
        public string EAN_13 { get; set; }
        public string COD_FORNITORE { get; set; }
        public string DESCRIZIONE_FORNITORE { get; set; }
        public string EAN_FORNITORE { get; set; }
        public string CODICE_ARTICOLO { get; set; }
        public string FLAG_X_CARICO { get; set; }
        public string PESO_UNITARIO { get; set; }
        public string SVILUPPO_ARTICOLO { get; set; }
        public string MAGAZZINO { get; set; }
        public string COD_BRICO { get; set; }
        public string SETTORE { get; set; }
        public string FILA { get; set; }
        public string PZ_X_MQ { get; set; }


        public int QUANTITA { get; set; }
        public bool APPROVATO { get; set; }

        public int FK_ARTICOLO_VENDITA { get; set; }

        public string LASTUPDATE { get; set; }
    }
}
