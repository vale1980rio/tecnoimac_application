﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("Movimentazioni")]
    public class Movimentazione_SQLite
    {
        [PrimaryKey]
        [AutoIncrement]
        public int ID { get; set; }
        public string ID_MOVIMENTAZIONE { get; set; }
        public string FK_MAGAZZINO_PARTENZA { get; set; }
        public string CODICE_MAGAZZINO_PARTENZA { get; set; }
        public string DESCRIZIONE_MAGAZZINO_PARTENZA { get; set; }
        public string FK_MAGAZZINO_ARRIVO { get; set; }
        public string CODICE_MAGAZZINO_ARRIVO { get; set; }
        public string DESCRIZIONE_MAGAZZINO_ARRIVO { get; set; }
        public DateTime DATA_CREAZIONE { get; set; }
    }
}
