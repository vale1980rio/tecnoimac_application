﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("Clienti")]
    public class Cliente_SQLite
    {
        [PrimaryKey]
        public int ID { get; set; }
        public string CLIENTE { get; set; }
        public string INDIRIZZO { get; set; }
        public string COMUNE { get; set; }
        public string PROVINCIA { get; set; }
        public string CAP { get; set; }
        public string IVACF { get; set; }
        public string CODICE { get; set; }

    }
}
