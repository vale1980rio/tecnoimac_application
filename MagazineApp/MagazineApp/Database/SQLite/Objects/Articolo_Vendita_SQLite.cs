﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("ArticoliVendita")]
    public class Articolo_Vendita_SQLite
    {
        [PrimaryKey][AutoIncrement]
        public int ID { get; set; }
        public string FK_VENDITA { get; set; }
        public string FK_ARTICOLO { get; set; }
        public int QUANTITA { get; set; }
        public bool APPROVATO { get; set; }
    }
}
