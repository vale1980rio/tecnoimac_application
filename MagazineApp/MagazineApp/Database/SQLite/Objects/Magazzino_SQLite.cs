﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Database.SQLite.Objects
{
    [Table("Magazzini")]
    public class Magazzino_SQLite
    {
        [PrimaryKey][AutoIncrement]
        public int ID { get; set; }
        public int IDMAGAZZINO { get; set; }
        public string RAND { get; set; }
        public string CODICE { get; set; }
        public string DESCRIZIONE { get; set; }
        public string USED { get; set; }
    }
}
