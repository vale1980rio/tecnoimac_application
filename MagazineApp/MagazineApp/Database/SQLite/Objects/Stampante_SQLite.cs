﻿using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


namespace MagazineApp.Database.SQLite.Objects
{
    [Table("Stampanti")]
    public class Stampante_SQLite
    {
        [PrimaryKey] [AutoIncrement]
        public int ID { get; set; }
        public string IP { get; set; }
        public string PORTA { get; set; }
        public string DESCRIZIONE { get; set; }
        public string DESCRIZIONE_2 { get; set; }


    }

}
