﻿using DevExpress.Mobile.DataGrid;
using DevExpress.Mobile.DataGrid.Theme;
using MagazineApp.DataGrid.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using Android.Widget;
using Java.IO;
using Java.Net;
using Android.Content;
using MagazineApp.Database.MySQL;
using MagazineApp.Interfaces;
using MagazineApp.DataGrid.Objects;
using Android.PrintServices;
using MagazineApp.Database.SQLite;
using MagazineApp.Database.SQLite.Objects;
using AutoMapper;
using MagazineApp.Mappings;
using System.Net.Sockets;
using System.Net;
using DevExpress.Mobile.DataGrid.Localization;
using System.Globalization;
using DevExpress.Mobile.Core;

using Syncfusion.Pdf;
using Syncfusion.Pdf.Parsing;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using Syncfusion.Drawing;
using DevExpress.Data;
using MagazineApp.Utility;

namespace MagazineApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VenditaPage : CarouselPage
    {

        Vendita_DEVEXPRESS vendita;
        Cliente_SQLite cliente;
        
        public string YourBoundString { get; set; }
        
        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<string, string>("MyApp", "NotifyMsg");
            base.OnDisappearing();
        }
        
        public VenditaPage(Vendita_DEVEXPRESS obj)
        {
            vendita = obj;

            #region CentralMessage
            MessagingCenter.Subscribe<string, string>("MyApp", "NotifyMsg", (sender, arg) =>
            {
                CERCA_ARTICOLO(arg.ToString());

            });

            #endregion

            InitializeComponent();

            EN_DI_PICKER();

            GridLocalizer.SetResource("MagazineApp.Localization.GridLocalizationRes", typeof(VenditaPage).GetTypeInfo().Assembly);

            Carica_Dati_Griglia();
            SQLite_Manager slm = new SQLite_Manager();

            string message = string.Empty;
            bool ok = false;

            cliente = slm.GET_CLIENTE_SQLite(vendita.FK_CLIENTE, ref message, ref ok);

            LB_CODICE.Text = obj.ID.ToString();
            LB_CLIENTE_2.Text = obj.CLIENTE;
            LB_INDIRIZZO.Text = obj.INDIRIZZO;

         

        }

        #region BUTTONS
        public async void BT_Stampa_Clicked(object sender, EventArgs args)
        {
            Print();
            await Navigation.PopToRootAsync();
        }
        public void BT_View_Clicked(object sender, EventArgs args)
        {
            viewPDF();
        }
        public void BT_Inserisci_Clicked(object sender, EventArgs args)
        {
            var articolo_SQLite = (Articolo_SQLite)pickerarticoli.SelectedItem;

            #region 

            if (articolo_SQLite != null)
            {
                SQLite_Manager slm = new SQLite_Manager();
                string message_1 = string.Empty;
                bool ok_1 = false;
                int quantita = Int32.Parse(articolo_SQLite.PZ_CONF.Replace(",00", ""));
                //articolo_SQLite.QUANTITA_RICHIESTA = quantita;
                slm.INSERT_ARTICOLO_VENDITA_SQLite(articolo_SQLite.ID, vendita.ID.ToString(), quantita, ref message_1, ref ok_1);
                if (ok_1)
                {
                    DevExpressArticoliRepository model_articoli = new DevExpressArticoliRepository(vendita.ID.ToString());
                    grid_articoli.BindingContext = model_articoli;
                    pickerarticoli.BindingContext = null;
                }
            }
            else
            {
                DisplayAlert("ARTICOLO", "Nessun Articolo Selezionato", "OK");
            }
            #endregion
            EN_DI_PICKER();
        }

        public void BT_Elimina_Click(object sender, EventArgs args)
        {
            DevExpressArticoliRepository model = (DevExpressArticoliRepository)grid_articoli.BindingContext;
            if (model.Articoli.Count > 0)
            {
                try
                {
                    Articolo_DEVEXPRESS obj = (Articolo_DEVEXPRESS)grid_articoli.SelectedDataObject;
                    if (obj != null)
                    {
                        string message = string.Empty;
                        bool ok = false;
                        int count = 0;
                        SQLite_Manager slm = new SQLite_Manager();

                        slm.DELETE_ARTICOLO_SQLite(obj.FK_ARTICOLO_VENDITA, ref message, ref ok);

                        if (ok)
                        {
                            //DevExpressArticoliRepository model_articoli = new DevExpressArticoliRepository(vendita.ID.ToString());
                            var modell = (DevExpressArticoliRepository)grid_articoli.BindingContext;

                            modell.Articoli.Remove(obj);
                        }
                    }
                    else
                    {
                        DisplayAlert("ARTICOLO", "Nessun Articolo Selezionato", "OK");
                    }
                }
                catch (Exception e)
                {
                    DisplayAlert("ARTICOLO", "Nessun Articolo Selezionato", "OK");
                }
            }
            else
            {
                DisplayAlert("ARTICOLO", "Nessun Articolo Selezionato", "OK");
            }
        }
        
        #endregion

        #region PRINT
        private string ADD_ZERO(string code)
        {
            int lunghezza = code.Length;
            int toadd = 6 - lunghezza;

            for (int i = 0; i < toadd; i++)
            {
                code = "0" + code;
            }

            return code;
        }
        
        public bool createPDF(string name)
        {
            try
            {
                if (((DevExpressArticoliRepository)grid_articoli.BindingContext).Articoli.Count > 0)
                {

                    #region DESIGN PAGE

                    //Load the existing PDF document.
                    Stream docStream = typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream("MagazineApp.Templates.DDT_VENDITE.pdf");
                    PdfLoadedDocument loadedDocument = new PdfLoadedDocument(docStream);

                    //Load the page

                    PdfLoadedPage loadedPage = loadedDocument.Pages[0] as PdfLoadedPage;
                    //Create the template from the page.

                    PdfTemplate template = loadedPage.CreateTemplate();

                    //Create a new PDF document

                    PdfDocument document = new PdfDocument();
                    document.PageSettings.SetMargins(2);
                    document.PageSettings.Size = PdfPageSize.A4;
                    //Add the page

                    PdfPage page = document.Pages.Add();

                    //Create the graphics

                    PdfGraphics graphics = page.Graphics;

                    //Draw the template


                    graphics.DrawPdfTemplate(template, PointF.Empty, new SizeF(PdfPageSize.A4.Width, PdfPageSize.A4.Height));

                    PdfFont font = new PdfStandardFont(PdfFontFamily.TimesRoman, 8);


                    #region DATI NON IN LOOP

                    graphics.DrawString(cliente.CLIENTE.ToString(), font, PdfBrushes.Black, new PointF(40, 160));
                    graphics.DrawString(cliente.INDIRIZZO.ToString(), font, PdfBrushes.Black, new PointF(40, 170));
                    graphics.DrawString(string.Format("{0} {1} {2}", cliente.CAP, cliente.COMUNE, cliente.PROVINCIA), font, PdfBrushes.Black, new PointF(40, 180));


                    graphics.DrawString(vendita.MAGAZZINO, font, PdfBrushes.Black, new PointF(498, 238));
                    graphics.DrawString(cliente.IVACF, font, PdfBrushes.Black, new PointF(270, 238));
                    graphics.DrawString(ADD_ZERO(cliente.ID.ToString()), font, PdfBrushes.Black, new PointF(212, 238));
                    graphics.DrawString(string.Format("{0:dd/MM/yyyy}", vendita.CREAZIONE), font, PdfBrushes.Black, new PointF(98, 238));
                    graphics.DrawString(vendita.ID.ToString(), font, PdfBrushes.Black, new PointF(40, 238));

                    #endregion



                    int count = 282;

                    foreach (var articolo in ((DevExpressArticoliRepository)grid_articoli.BindingContext).Articoli)
                    {

                        graphics.DrawString(articolo.DESCRIZIONE, font, PdfBrushes.Black, new PointF(155, count));

                        graphics.DrawString(articolo.CODICE_ARTICOLO, font, PdfBrushes.Black, new PointF(40, count));

                        graphics.DrawString("PZ.", font, PdfBrushes.Black, new PointF(440, count));

                        graphics.DrawString(articolo.QUANTITA, font, PdfBrushes.Black, new PointF(498, count));

                        count = count + 10;
                    }

                    #region CALCOLA PESO
                    decimal peso_totale = 0;
                    try
                    {
                        foreach (var articolo in ((DevExpressArticoliRepository)grid_articoli.BindingContext).Articoli)
                        {
                            decimal peso = Decimal.Parse(articolo.PESO_UNITARIO) * Int32.Parse(articolo.QUANTITA);
                            peso_totale = peso + peso_totale;
                        }
                        graphics.DrawString(peso_totale.ToString(), font, PdfBrushes.Black, new PointF(100, 650));
                        graphics.DrawString(string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now), font, PdfBrushes.Black, new PointF(215, 650));
                    }
                    catch
                    {
                        peso_totale = 0;
                    }
                    #endregion


                    #endregion

                    MemoryStream stream = new MemoryStream();

                    //Save the document.
                    document.Save(stream);

                    var dbName_2 = name;
                    var path_2 = Path.Combine("/sdcard/", dbName_2);

                    //path_2 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName_2);

                    Xamarin.Forms.DependencyService.Get<ISave>().SaveTextAsync(path_2, "application/pdf", stream);
                    //Close the document.
                    document.Close(true);

                    return true;

                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }

        public bool viewPDF()
        {
            try
            {
                if (((DevExpressArticoliRepository)grid_articoli.BindingContext).Articoli.Count > 0)
                {

                    #region DESIGN PAGE

                    //Load the existing PDF document.
                    Stream docStream = typeof(App).GetTypeInfo().Assembly.GetManifestResourceStream("MagazineApp.Templates.DDT_VENDITE.pdf");
                    PdfLoadedDocument loadedDocument = new PdfLoadedDocument(docStream);

                    //Load the page

                    PdfLoadedPage loadedPage = loadedDocument.Pages[0] as PdfLoadedPage;
                    //Create the template from the page.

                    PdfTemplate template = loadedPage.CreateTemplate();

                    //Create a new PDF document

                    PdfDocument document = new PdfDocument();
                    document.PageSettings.SetMargins(2);
                    document.PageSettings.Size = PdfPageSize.A4;
                    //Add the page

                    PdfPage page = document.Pages.Add();

                    //Create the graphics

                    PdfGraphics graphics = page.Graphics;

                    //Draw the template


                    graphics.DrawPdfTemplate(template, PointF.Empty, new SizeF(PdfPageSize.A4.Width, PdfPageSize.A4.Height));

                    PdfFont font = new PdfStandardFont(PdfFontFamily.TimesRoman, 8);


                    #region DATI NON IN LOOP

                    graphics.DrawString(cliente.CLIENTE.ToString(), font, PdfBrushes.Black, new PointF(40, 160));
                    graphics.DrawString(cliente.INDIRIZZO.ToString(), font, PdfBrushes.Black, new PointF(40, 170));
                    graphics.DrawString(string.Format("{0} {1} {2}", cliente.CAP, cliente.COMUNE, cliente.PROVINCIA), font, PdfBrushes.Black, new PointF(40, 180));


                    graphics.DrawString(vendita.MAGAZZINO, font, PdfBrushes.Black, new PointF(498, 238));
                    graphics.DrawString(cliente.IVACF, font, PdfBrushes.Black, new PointF(270, 238));
                    graphics.DrawString(ADD_ZERO(cliente.ID.ToString()), font, PdfBrushes.Black, new PointF(212, 238));
                    graphics.DrawString(string.Format("{0:dd/MM/yyyy}", vendita.CREAZIONE), font, PdfBrushes.Black, new PointF(98, 238));
                    graphics.DrawString(vendita.ID.ToString(), font, PdfBrushes.Black, new PointF(40, 238));

                    #endregion



                    int count = 282;

                    foreach (var articolo in ((DevExpressArticoliRepository)grid_articoli.BindingContext).Articoli)
                    {

                        graphics.DrawString(articolo.DESCRIZIONE, font, PdfBrushes.Black, new PointF(155, count));

                        graphics.DrawString(articolo.CODICE_ARTICOLO, font, PdfBrushes.Black, new PointF(40, count));

                        graphics.DrawString("PZ.", font, PdfBrushes.Black, new PointF(440, count));

                        graphics.DrawString(articolo.QUANTITA, font, PdfBrushes.Black, new PointF(498, count));

                        count = count + 10;
                    }

                    #region CALCOLA PESO
                    decimal peso_totale = 0;
                    try
                    {
                        foreach (var articolo in ((DevExpressArticoliRepository)grid_articoli.BindingContext).Articoli)
                        {
                            decimal peso = Decimal.Parse(articolo.PESO_UNITARIO) * Int32.Parse(articolo.QUANTITA);
                            peso_totale = peso + peso_totale;
                        }
                        graphics.DrawString(peso_totale.ToString(), font, PdfBrushes.Black, new PointF(100, 650));
                        graphics.DrawString(string.Format("{0:dd/MM/yyyy HH:mm}", DateTime.Now), font, PdfBrushes.Black, new PointF(215, 650));
                    }
                    catch
                    {
                        peso_totale = 0;
                    }
                    #endregion


                    #endregion

                    MemoryStream stream = new MemoryStream();

                    //Save the document.
                    document.Save(stream);

                    var dbName_2 = "view.pdf";
                    var path_2 = Path.Combine("/sdcard/", dbName_2);

                    //path_2 = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName_2);

                    Xamarin.Forms.DependencyService.Get<ISave>().ViewTextAsync(path_2, "application/pdf", stream);
                    //Close the document.
                    document.Close(true);

                    return true;

                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }

        }

        private void Print()
        {
            Stampante_DEVEXPRESS obj_stampa = (Stampante_DEVEXPRESS)grid_stampa.SelectedDataObject;
            
            var address = obj_stampa.DESCRIZIONE;
            
            string dbName = string.Format("{0:yyyyMMdd_mmss}_{1}_vendita.pdf",DateTime.Now ,vendita.ID);
            bool ok_pdf = createPDF(dbName);

            if (ok_pdf)
            {
                var path = Path.Combine("/sdcard/", dbName);
                //var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
                byte[] fileBytes = System.IO.File.ReadAllBytes(path);
                string esitoperazione = DependencyService.Get<IWebService>().Print(fileBytes, obj_stampa.DESCRIZIONE, dbName);
                DependencyService.Get<IMessage>().LongAlert(esitoperazione);
            }
            else
            {
                DependencyService.Get<IMessage>().LongAlert("Errore Creazione File PDF Da Inviare In Stampa o Nessun Articolo Presente in Vendita.");
              
            }
        }
        #endregion

        #region GRID
        object GetTemplate()
        {
            DevExpressArticoliRepository model_articoli = new DevExpressArticoliRepository(vendita.ID.ToString());
            return new CustomEditFormContent() { ViewModel = model_articoli };
        }

        private void Carica_Dati_Griglia()
        {

            DevExpressStampantiRepository model_stampanti = new DevExpressStampantiRepository();
            grid_stampa.BindingContext = model_stampanti;


            DevExpressArticoliRepository model_articoli = new DevExpressArticoliRepository(vendita.ID.ToString());
            grid_articoli.BindingContext = model_articoli;

            DataTemplate editFormContentTemplate = new DataTemplate(GetTemplate);
            grid_articoli.EditFormContent = editFormContentTemplate;
            grid_articoli.RowEditMode = RowEditMode.Popup;

        }


        void OnCustomizeCell(CustomizeCellEventArgs e)
        {
            try
            {


                if (e.FieldName == "QUANTITA")
                {

                    int index = e.RowHandle;

                    var approvato = grid_articoli.GetCellValue(index, "APPROVATO");

                    if ((approvato.ToString() != "0") && (approvato.ToString() != "False"))
                    {
                        e.BackgroundColor = Xamarin.Forms.Color.LightGreen;

                        if (e.Value.ToString() == "0")
                        {
                            e.BackgroundColor = Xamarin.Forms.Color.PaleVioletRed;
                        }
                        else
                        {
                            e.BackgroundColor = Xamarin.Forms.Color.LightGreen;
                        }
                    }
                    else
                        e.BackgroundColor = Xamarin.Forms.Color.LightYellow;

                    e.Handled = true;
                }
            }
            catch { }
        }

        public void rowTap_grid_articoli(object sender, RowTapEventArgs args)
        {
            try
            {
                if (args.FieldName == "QUANTITA")
                {
                    CellIndex cell = new CellIndex(args.RowHandle, args.FieldName);
                    grid_articoli.OpenEditor(cell);
                }
            }
            catch (Exception ext)
            {

            }
        }
        #endregion

        #region UTILITY

        public void test(object sender, EventArgs args)
        {
            string message = string.Empty;
            bool ok = false;
            DevExpressArticoliRepository model_articoli = (DevExpressArticoliRepository)grid_articoli.BindingContext;
            SQLite_Manager slm = new SQLite_Manager();
            slm.UPDATE_ALL_VENDITE_SQLite(model_articoli.Articoli, ref message, ref ok);
        }


        private void EN_DI_PICKER()
        {

            var model = (ArticoloPickerModel)pickerarticoli.BindingContext;

            if (model is null)
            {
                pickerarticoli.IsEnabled = false;
            }
            else if (model.Articoli.Count() == 0)
            {
                pickerarticoli.IsEnabled = false;
            }
            else if (model.Articoli.Count() < 0)
            {
                pickerarticoli.IsEnabled = false;
            }
            else if (model.Articoli.Count() > 0)
            {
                pickerarticoli.IsEnabled = true;
            }
        }

        void CERCA_ARTICOLO(string code)
        {
            if ((!string.IsNullOrWhiteSpace(code) && (!string.IsNullOrEmpty(code))))
            {

                if (code.Trim() == "0000000000000")
                {
                    DisplayAlert("INFO: ", "Codidice '0000000000000' Non Ammesso.", "Ok");
                    return;
                }

                string message = string.Empty;
                bool ok = false;
                int count = 0;
                SQLite_Manager slm = new SQLite_Manager();
                List<Articolo_SQLite> articolo_SQLite = slm.GET_ARTICOLI_SQLite(code, vendita.magazzino, ref message, ref ok, ref count);

                if (ok)
                {
                    if (articolo_SQLite.Count == 0)
                    {
                        DisplayAlert("INFO: ", "Nessun Articolo Trovato.", "Ok");
                    }
                    else if (articolo_SQLite.Count == 1)
                    {
                        #region UNO
                        string message_1 = string.Empty;
                        bool ok_1 = false;
                        int quantita = Int32.Parse(articolo_SQLite[0].PZ_CONF.Replace(",00", ""));
                        //articolo_SQLite.QUANTITA_RICHIESTA = quantita;
                        slm.INSERT_ARTICOLO_VENDITA_SQLite(articolo_SQLite[0].ID, vendita.ID.ToString(), quantita, ref message_1, ref ok_1);
                        if (ok_1)
                        {
                            DevExpressArticoliRepository model_articoli = new DevExpressArticoliRepository(vendita.ID.ToString());
                            grid_articoli.BindingContext = model_articoli;
                        }
                        #endregion
                    }
                    else if (articolo_SQLite.Count > 1)
                    {
                        #region +DI UNO

                        ArticoloPickerModel model = new ArticoloPickerModel();

                        model.Articoli = new List<Articolo_SQLite>();

                        foreach (var t in articolo_SQLite)
                        {
                            model.Articoli.Add(t);

                        }
                        pickerarticoli.BindingContext = model;
                        pickerarticoli.SelectedIndex = 0;
                        DisplayAlert("INFO: ", "Più articoli con lo stesso codice trovati  nel magazzino '" + vendita.magazzino + "'.\nSelezionare l'articolo desiderato dal menu a tendina", "Ok");
                        #endregion
                    }
                }
                else
                {
                    DisplayAlert("INFO: ", message, "Ok");

                }
            }
            else
            {
                DisplayAlert("INFO: ", "Errore Lettura Codice.", "Ok");
            }

            EN_DI_PICKER();
        }

        #endregion

        #region CULTURE

        void OnDefaultCulture(object sender, EventArgs e)
        {
            SetCulture(new CultureInfo("it-IT"));
        }
        void OnEnglishCulture(object sender, EventArgs e)
        {
            SetCulture(new CultureInfo("en-US"));
        }
        void OnFrenchCulture(object sender, EventArgs e)
        {
            SetCulture(new CultureInfo("fr-FR"));
        }
        void OnGermanCulture(object sender, EventArgs e)
        {
            SetCulture(new CultureInfo("de-DE"));
        }
        void OnSpanishCulture(object sender, EventArgs e)
        {
            SetCulture(new CultureInfo("es-ES"));
        }
        void OnRussianCulture(object sender, EventArgs e)
        {
            SetCulture(new CultureInfo("ru-RU"));
        }



        void SetCulture(CultureInfo culture)
        {
            IGlobalizationService service = GlobalServices.Instance.GetService<IGlobalizationService>();
            if (service == null)
                return;

            if (culture == null)
                culture = service.CurrentOSCulture;

            service.CurrentCulture = culture;
            service.CurrentUICulture = culture;

            GridLocalizer.ResetCache();

            grid_articoli.Redraw(true);
        }


        #endregion
    }
}