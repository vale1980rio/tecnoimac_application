﻿using AutoMapper;
using MagazineApp.Database.SQLite.Objects;
using MagazineApp.DataGrid.Objects;

namespace MagazineApp.Mappings
{
    #region ARTICOLO
   
   
    public class Articolo_DEVEXPRESS_Articolo_SQLite : Profile
    {
        public Articolo_DEVEXPRESS_Articolo_SQLite()
        {
            IMappingExpression<Articolo_DEVEXPRESS, Articolo_SQLite> mappingExpression;

            mappingExpression = CreateMap<Articolo_DEVEXPRESS, Articolo_SQLite>();

            mappingExpression.ForMember(d => d.APPROVATO, o => o.MapFrom(s => s.APPROVATO));
            mappingExpression.ForMember(d => d.CODICE_ARTICOLO, o => o.MapFrom(s => s.CODICE_ARTICOLO));
            mappingExpression.ForMember(d => d.COD_BRICO, o => o.MapFrom(s => s.COD_BRICO));
            mappingExpression.ForMember(d => d.COD_FORNITORE, o => o.MapFrom(s => s.COD_FORNITORE));
            mappingExpression.ForMember(d => d.DESCRIZIONE, o => o.MapFrom(s => s.DESCRIZIONE));
            mappingExpression.ForMember(d => d.DESCRIZIONE_FORNITORE, o => o.MapFrom(s => s.DESCRIZIONE_FORNITORE));
            mappingExpression.ForMember(d => d.EAN_128, o => o.MapFrom(s => s.EAN_128));
            mappingExpression.ForMember(d => d.EAN_13, o => o.MapFrom(s => s.EAN_13));
            mappingExpression.ForMember(d => d.EAN_FORNITORE, o => o.MapFrom(s => s.EAN_FORNITORE));
            mappingExpression.ForMember(d => d.FILA, o => o.MapFrom(s => s.FILA));
            mappingExpression.ForMember(d => d.FLAG_X_CARICO, o => o.MapFrom(s => s.FLAG_X_CARICO));
            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.LASTUPDATE, o => o.MapFrom(s => s.LASTUPDATE));
            mappingExpression.ForMember(d => d.MAGAZZINO, o => o.MapFrom(s => s.MAGAZZINO));
            mappingExpression.ForMember(d => d.PESO_UNITARIO, o => o.MapFrom(s => s.PESO_UNITARIO));
            mappingExpression.ForMember(d => d.PZ_CONF, o => o.MapFrom(s => s.PZ_CONF));
            mappingExpression.ForMember(d => d.PZ_X_MQ, o => o.MapFrom(s => s.PZ_X_MQ));
            mappingExpression.ForMember(d => d.QUANTITA, o => o.MapFrom(s => s.QUANTITA));
            mappingExpression.ForMember(d => d.SETTORE, o => o.MapFrom(s => s.SETTORE));
            mappingExpression.ForMember(d => d.SVILUPPO_ARTICOLO, o => o.MapFrom(s => s.SVILUPPO_ARTICOLO));
        }
    }
    public class Articolo_SQLite_Articolo_DEVEXPRESS : Profile
    {
        public Articolo_SQLite_Articolo_DEVEXPRESS()
        {
            IMappingExpression<Articolo_SQLite, Articolo_DEVEXPRESS> mappingExpression;

            mappingExpression = CreateMap<Articolo_SQLite, Articolo_DEVEXPRESS>();

            mappingExpression.ForMember(d => d.APPROVATO, o => o.MapFrom(s => s.APPROVATO));
            mappingExpression.ForMember(d => d.CODICE_ARTICOLO, o => o.MapFrom(s => s.CODICE_ARTICOLO));
            mappingExpression.ForMember(d => d.COD_BRICO, o => o.MapFrom(s => s.COD_BRICO));
            mappingExpression.ForMember(d => d.COD_FORNITORE, o => o.MapFrom(s => s.COD_FORNITORE));
            mappingExpression.ForMember(d => d.DESCRIZIONE, o => o.MapFrom(s => s.DESCRIZIONE));
            mappingExpression.ForMember(d => d.DESCRIZIONE_FORNITORE, o => o.MapFrom(s => s.DESCRIZIONE_FORNITORE));
            mappingExpression.ForMember(d => d.EAN_128, o => o.MapFrom(s => s.EAN_128));
            mappingExpression.ForMember(d => d.EAN_13, o => o.MapFrom(s => s.EAN_13));
            mappingExpression.ForMember(d => d.EAN_FORNITORE, o => o.MapFrom(s => s.EAN_FORNITORE));
            mappingExpression.ForMember(d => d.FILA, o => o.MapFrom(s => s.FILA));
            mappingExpression.ForMember(d => d.FLAG_X_CARICO, o => o.MapFrom(s => s.FLAG_X_CARICO));
            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.LASTUPDATE, o => o.MapFrom(s => s.LASTUPDATE));
            mappingExpression.ForMember(d => d.MAGAZZINO, o => o.MapFrom(s => s.MAGAZZINO));
            mappingExpression.ForMember(d => d.PESO_UNITARIO, o => o.MapFrom(s => s.PESO_UNITARIO));
            mappingExpression.ForMember(d => d.PZ_CONF, o => o.MapFrom(s => s.PZ_CONF));
            mappingExpression.ForMember(d => d.PZ_X_MQ, o => o.MapFrom(s => s.PZ_X_MQ));
            mappingExpression.ForMember(d => d.QUANTITA, o => o.MapFrom(s => s.QUANTITA));
            mappingExpression.ForMember(d => d.SETTORE, o => o.MapFrom(s => s.SETTORE));
            mappingExpression.ForMember(d => d.SVILUPPO_ARTICOLO, o => o.MapFrom(s => s.SVILUPPO_ARTICOLO));
         
        }
    }
    #endregion

    #region STAMPANTE
    public class Stampante_SQLite__Stampante_DEVEXPRESS_Profile : Profile
    {
        public Stampante_SQLite__Stampante_DEVEXPRESS_Profile()
        {
            IMappingExpression<Stampante_SQLite, Stampante_DEVEXPRESS> mappingExpression;

            mappingExpression = CreateMap<Stampante_SQLite, Stampante_DEVEXPRESS>();

            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.IP, o => o.MapFrom(s => s.IP));
            mappingExpression.ForMember(d => d.PORTA, o => o.MapFrom(s => s.PORTA));
            mappingExpression.ForMember(d => d.DESCRIZIONE, o => o.MapFrom(s => s.DESCRIZIONE));
            mappingExpression.ForMember(d => d.DESCRIZIONE_2, o => o.MapFrom(s => s.DESCRIZIONE_2));
        }
    }
    public class Stampante_DEVEXPRESS__Stampante_SQLite_Profile : Profile
    {
        public Stampante_DEVEXPRESS__Stampante_SQLite_Profile()
        {
            IMappingExpression<Stampante_DEVEXPRESS, Stampante_SQLite> mappingExpression;

            mappingExpression = CreateMap<Stampante_DEVEXPRESS, Stampante_SQLite>();

            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.IP, o => o.MapFrom(s => s.IP));
            mappingExpression.ForMember(d => d.PORTA, o => o.MapFrom(s => s.PORTA));
            mappingExpression.ForMember(d => d.DESCRIZIONE, o => o.MapFrom(s => s.DESCRIZIONE));
            mappingExpression.ForMember(d => d.DESCRIZIONE_2, o => o.MapFrom(s => s.DESCRIZIONE_2));
        }
    }
    #endregion

    #region CLIENTE
    
    public class Cliente_SQLite__Cliente_DEVEXPRESS_Profile : Profile
    {
        public Cliente_SQLite__Cliente_DEVEXPRESS_Profile()
        {
            IMappingExpression<Cliente_SQLite, Cliente_DEVEXPRESS> mappingExpression;

            mappingExpression = CreateMap<Cliente_SQLite, Cliente_DEVEXPRESS>();

            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.CLIENTE, o => o.MapFrom(s => s.CLIENTE));
            mappingExpression.ForMember(d => d.INDIRIZZO, o => o.MapFrom(s => s.INDIRIZZO));
            mappingExpression.ForMember(d => d.PROVINCIA, o => o.MapFrom(s => s.PROVINCIA));
            mappingExpression.ForMember(d => d.COMUNE, o => o.MapFrom(s => s.COMUNE));
            mappingExpression.ForMember(d => d.CAP, o => o.MapFrom(s => s.CAP));
            mappingExpression.ForMember(d => d.IVACF, o => o.MapFrom(s => s.IVACF));
        }
    }
    public class Cliente_DEVEXPRESS__Cliente_SQLite_Profile : Profile
    {
        public Cliente_DEVEXPRESS__Cliente_SQLite_Profile()
        {
            IMappingExpression<Cliente_DEVEXPRESS, Cliente_SQLite> mappingExpression;

            mappingExpression = CreateMap<Cliente_DEVEXPRESS, Cliente_SQLite>();

            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.CLIENTE, o => o.MapFrom(s => s.CLIENTE));
            mappingExpression.ForMember(d => d.INDIRIZZO, o => o.MapFrom(s => s.INDIRIZZO));
            mappingExpression.ForMember(d => d.PROVINCIA, o => o.MapFrom(s => s.PROVINCIA));
            mappingExpression.ForMember(d => d.COMUNE, o => o.MapFrom(s => s.COMUNE));
            mappingExpression.ForMember(d => d.CAP, o => o.MapFrom(s => s.CAP));
            mappingExpression.ForMember(d => d.IVACF, o => o.MapFrom(s => s.IVACF));
        }
    }
   
    #endregion

    #region VENDITA
   

    public class Vendita_SQLite__Vendita_DEVEXPRESS_Profile : Profile
    {
        public Vendita_SQLite__Vendita_DEVEXPRESS_Profile()
        {
            IMappingExpression<Vendita_SQLite, Vendita_DEVEXPRESS> mappingExpression;

            mappingExpression = CreateMap<Vendita_SQLite, Vendita_DEVEXPRESS>();
            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.FK_CLIENTE, o => o.MapFrom(s => s.FK_CLIENTE));
            mappingExpression.ForMember(d => d.DESCRIZIONE, o => o.MapFrom(s => s.DESCRIZIONE_VENDITA));
            mappingExpression.ForMember(d => d.CLIENTE, o => o.MapFrom(s => s.CLIENTE));
            mappingExpression.ForMember(d => d.INDIRIZZO, o => o.MapFrom(s => s.INDIRIZZO_CLIENTE));
            mappingExpression.ForMember(d => d.MAGAZZINO, o => o.MapFrom(s => s.MAGAZZINO));
            mappingExpression.ForMember(d => d.CREAZIONE, o => o.MapFrom(s => s.CREAZIONE));
        }
    }

    public class Vendita_DEVEXPRESS__Vendita_SQLite_Profile : Profile
    {
        public Vendita_DEVEXPRESS__Vendita_SQLite_Profile()
        {
            IMappingExpression<Vendita_DEVEXPRESS, Vendita_SQLite> mappingExpression;

            mappingExpression = CreateMap<Vendita_DEVEXPRESS, Vendita_SQLite>();
            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.FK_CLIENTE, o => o.MapFrom(s => s.FK_CLIENTE));
            mappingExpression.ForMember(d => d.DESCRIZIONE_VENDITA, o => o.MapFrom(s => s.DESCRIZIONE));
            mappingExpression.ForMember(d => d.CLIENTE, o => o.MapFrom(s => s.CLIENTE));
            mappingExpression.ForMember(d => d.INDIRIZZO_CLIENTE, o => o.MapFrom(s => s.INDIRIZZO));
            mappingExpression.ForMember(d => d.MAGAZZINO, o => o.MapFrom(s => s.MAGAZZINO));
            mappingExpression.ForMember(d => d.CREAZIONE, o => o.MapFrom(s => s.CREAZIONE));
        }
    }

    #endregion

    #region MAGAZZINO
    
    public class Magazzino_DEVEXPRESS__Magazzino_SQLite_Profile : Profile
    {
        public Magazzino_DEVEXPRESS__Magazzino_SQLite_Profile()
        {
            IMappingExpression<Magazzino_DEVEXPRESS, Magazzino_SQLite> mappingExpression;

            mappingExpression = CreateMap<Magazzino_DEVEXPRESS, Magazzino_SQLite>();
            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.IDMAGAZZINO, o => o.MapFrom(s => s.IDMAGAZZINO));
            mappingExpression.ForMember(d => d.DESCRIZIONE, o => o.MapFrom(s => s.DESCRIZIONE));
            mappingExpression.ForMember(d => d.CODICE, o => o.MapFrom(s => s.CODICE));
            mappingExpression.ForMember(d => d.RAND, o => o.MapFrom(s => s.RAND));
            mappingExpression.ForMember(d => d.USED, o => o.MapFrom(s => s.USED));
        }
    }

    public class Magazzino_SQLite__Magazzino_DEVEXPRESS_Profile : Profile
    {
        public Magazzino_SQLite__Magazzino_DEVEXPRESS_Profile()
        {
            IMappingExpression<Magazzino_SQLite,Magazzino_DEVEXPRESS > mappingExpression;

            mappingExpression = CreateMap<Magazzino_SQLite, Magazzino_DEVEXPRESS>();
            mappingExpression.ForMember(d => d.ID, o => o.MapFrom(s => s.ID));
            mappingExpression.ForMember(d => d.IDMAGAZZINO, o => o.MapFrom(s => s.IDMAGAZZINO));
            mappingExpression.ForMember(d => d.DESCRIZIONE, o => o.MapFrom(s => s.DESCRIZIONE));
            mappingExpression.ForMember(d => d.CODICE, o => o.MapFrom(s => s.CODICE));
            mappingExpression.ForMember(d => d.RAND, o => o.MapFrom(s => s.RAND));
            mappingExpression.ForMember(d => d.USED, o => o.MapFrom(s => s.USED));
        }
    }
    #endregion
}
