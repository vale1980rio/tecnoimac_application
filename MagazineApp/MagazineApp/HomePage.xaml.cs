﻿



using Android.Content;
using MagazineApp.Database.MySQL;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MagazineApp.Database.SQLite;
using MagazineApp.Interfaces;
using System.IO;
using System.Text;
using MagazineApp.Database.SQLite.Objects;
using MagazineApp.Utility;
using System.Threading.Tasks;
using MagazineApp.Database.FTP;
using System.Net.Sockets;
using System.Net;
using System.Reflection;
using System.ServiceModel;


namespace MagazineApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {

     
        public HomePage()
        {


            string message = string.Empty;
            bool ok = false;
            string message_1 = string.Empty;
            bool ok_1 = false;
            Context context = Android.App.Application.Context;
            InitializeComponent();
         
            SQLite_Manager slm = new SQLite_Manager();

            slm.DEFAULT_DEVICE_SETTINGS_SQLite(ref message, ref ok);
            UltimoAggiornamento(ref message_1, ref ok_1);

        }

        #region BUTTONS


        public async void OnClick_BT_Movimentazione(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new MovimentazionaManagerPage());
        }
        public async void OnClick_BT_Vendita(object sender, EventArgs args)
        {
          
           
          await Navigation.PushAsync(new VenditaManagerPage());
        }
        public async void OnClick_BT_Settings(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new LoginPage());
        }
        
        public void OnClick_BT_Update(object sender, EventArgs args)
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            MySQL_Settings_SQLite settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);

            bool ok_FTP = CheckLanStatus.CheckHost(settings.SERVER, 3306);

            if (!ok_FTP)
            {
                DisplayAlert("Errore", "Server Non Raggiungibile.", "OK");
                return;
            }

            bool continua = CheckVendite_Movimentazioni();

            if (!continua)
            {
                DisplayAlert("Errore", "Vendite e trasferimenti non ancora inviati al server. \n Prima di aggiornare i dati nel dispositivo premere il tasto\n 'Invia Dati'", "OK");
                return;
            }

            try
            {
                bool ok_0 = false;
                bool ok_1 = false;
                bool ok_2 = false;
                bool ok_3 = false;

                int articoli_count = 0;

                string message_0 = "Nessun Errore";
                string message_1 = "Nessun Errore";
                string message_2 = "Nessun Errore";
                string message_3 = "Nessun Errore";



                //Prendo i dati dal server in remoto MySQL


                //Metto i dati dentro SQLLite
                SQLite_Manager SQLite = new SQLite_Manager();


                var lista_articoli = FTP_Manager.GET_ALL_ARTICOLI_FTP(ref message_1, ref ok_1);
                var lista_clienti = FTP_Manager.GET_ALL_CLIENTI_FTP(ref message_2, ref ok_2);
                var lista_magazzini = FTP_Manager.GET_ALL_MAGAZZINI_FTP(ref message_3, ref ok_3);

                if (ok_1)
                {
                    SQLite.ADD_ARTICOLI_FROM_SQLite(lista_articoli, ref message_1, ref ok_1, ref articoli_count);
                }

                if (ok_2)
                {
                    SQLite.ADD_CLIENTI_SQLite(lista_clienti, ref message_2, ref ok_2);
                }

                if (ok_3)
                {
                    SQLite.ADD_MAGAZZINI_SQLite(lista_magazzini, ref message_3, ref ok_3);
                }

                if ((ok_1) && (ok_2) && (ok_3))
                {
                    UltimoAggiornamento(ref message_0, ref ok_0);

                    if (ok_0)
                    {
                        DisplayAlert("INFO", "Dati caricati correttamente.", "OK");
                    }
                    else
                    {
                        DisplayAlert("ERRORE", message_0, "OK");
                    }
                }
                else
                {
                    string messages = "ARTICOLI: " + message_1 + System.Environment.NewLine + " CLIENTI: " + message_2 + System.Environment.NewLine + " MAGAZZINI: " + message_3;
                    DisplayAlert("ERRORE", messages, "OK");
                }

            }
            catch (Exception e)
            {
                DisplayAlert("ERRORE", e.Message, "OK");
            }

        }
        public void OnClick_BT_FTP(object sender, EventArgs args)
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            MySQL_Settings_SQLite settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);

            bool ok_FTP = CheckLanStatus.CheckHost(settings.SERVER, 3306);

            if (!ok_FTP)
            {
                DisplayAlert("Errore", "Server Non Raggiungibile.", "OK");
                return;
            }



            bool movimentazioni_ok = CHECK_MOVIMENTAZIONI_FOR_TXT();
            bool vendite_ok = CHECK_VENDITE_FOR_TXT();

            if ((movimentazioni_ok) && (vendite_ok))
            {


                string filename_vendita = "VENDITE" + ".txt";
                createTXT_VENDITA(filename_vendita);

                string filename_movimentazioni = "TRASFERIMENTI" + ".txt";
                createTXT_MOVIMENTAZIONE(filename_movimentazioni);

                bool ok_vendita = uploadFTP(filename_vendita);
                bool ok_movimentazioni = uploadFTP(filename_movimentazioni);

                if ((ok_vendita) && (ok_movimentazioni))
                {
                    DisplayAlert("INFO", "Files Inviati Correttamente.", "OK");
                }
                else
                {
                    DisplayAlert("Errore", "Files NON  Inviati Correttamente.", "OK");
                }
            }
            else
            {
                DisplayAlert("Errore", "Dati vendite o trasferimenti incompleti", "OK");
            }

        }
        #endregion

        #region TASKS
        private async Task<bool> ShowIndicator()
        {
            return await Task<bool>.Run(() =>
            {
                try
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                      
                    });

                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        private async Task<bool> Upload()
        {
            return await Task<bool>.Run(() =>
            {
                try
                {
                    string message = string.Empty;
                    bool ok = false;
                    SQLite_Manager slm = new SQLite_Manager();
                    MySQL_Settings_SQLite settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);

                    bool ok_FTP = CheckLanStatus.CheckHost(settings.SERVER, 21);

                    if (!ok_FTP)
                    {
                        return false;
                    }


                    bool movimentazioni_ok = CHECK_MOVIMENTAZIONI_FOR_TXT();
                    bool vendite_ok = CHECK_VENDITE_FOR_TXT();

                    if ((movimentazioni_ok) && (vendite_ok))
                    {


                        string filename_vendita = "VENDITE" + ".txt";
                        createTXT_VENDITA(filename_vendita);

                        string filename_movimentazioni = "TRASFERIMENTI" + ".txt";
                        createTXT_MOVIMENTAZIONE(filename_movimentazioni);

                        uploadFTP(filename_vendita);
                        uploadFTP(filename_movimentazioni);
                    }
                    else
                    {
                        DisplayAlert("Errore", "Dati.", "OK");
                    }


                    return true;
                }
                catch (Exception ext)
                {
                    return false;
                }
            });
        }
        private async Task<bool> HideIndicator()
        {
            return await Task<bool>.Run(() =>
            {
                try
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                       
                    });

                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        #endregion

        #region UTILITY
        private void UltimoAggiornamento(ref string message, ref bool ok)
        {
            SQLite_Manager SQLite = new SQLite_Manager();
            string message_SQLite = string.Empty;
            bool ok_SQLite = false;

            string data = SQLite.GET_LAST_UPDATE_ADD_ARTICOLI(ref message_SQLite, ref ok_SQLite);

            if (ok_SQLite)
            {
                LB_Message.Text = "Ultimo Aggiornamento: " + data;
            }
            else
            {
                LB_Message.Text = "Aggiornare Dati.";
            }

            message = message_SQLite;
            ok = ok_SQLite;
        }
        private string ADD_ZERO(string code)
        {
            int lunghezza = code.Length;
            int toadd = 6 - lunghezza;

            for (int i = 0; i < toadd; i++)
            {
                code = "0" + code;
            }

            return code;
        }

        private bool CheckVendite_Movimentazioni()
        {
            SQLite_Manager slm = new SQLite_Manager();
            int movimentazioni = slm.MOVIMENTAZIONI_COUNT();
            int vendite = slm.VENDITE_COUNT();

            if ((movimentazioni == 0) && (vendite == 0))
            { return true; }
            else
            { return false; }
        }

        #endregion

        #region UPLOAD 
        private bool uploadFTP(string file)
        {
            try
            {
                string message = string.Empty;
                bool ok = false;
                SQLite_Manager slm = new SQLite_Manager();

                MySQL_Settings_SQLite settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);



                var path = Path.Combine("/sdcard/", file);
                bool ok_2 = false;
                DependencyService.Get<IFtpWebRequest>().append(ref ok_2, "ftp://" + settings.FTP_SERVER, file);
                return true;
                //DisplayAlert("Download", DependencyService.Get<IFtpWebRequest>().download("ftp://192.168.151.1/pippo.txt", path), "Ok");
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private void createTXT_VENDITA(string filename)
        {
            var dbName = filename;
            var path = Path.Combine("/sdcard/", dbName);

            using (FileStream aFile = new FileStream(path, FileMode.Create, FileAccess.Write))

            using (StreamWriter sw = new StreamWriter(aFile))
            {
                SQLite_Manager slm = new SQLite_Manager();

                string message = string.Empty;
                bool ok = false;

                var vendite = slm.GET_ALL_VENDITE_SQLite(ref message, ref ok);


                if (ok)
                {
                    foreach (var vendita in vendite)
                    {

                        //Prendo il cliente associato alla vendita
                        Cliente_SQLite cliente = slm.GET_CLIENTE_SQLite(vendita.fk_Cliente, ref message, ref ok);

                        decimal quantita = 0;


                        //Calcolo il peso della vendita
                        var articoli = slm.GET_ARTICOLI_BYCODE(vendita.ID.ToString(), ref message, ref ok);


                        foreach (var articolo in articoli)
                        {
                            quantita = quantita + (Int32.Parse(articolo.QUANTITA) * Decimal.Parse(articolo.PESO_UNITARIO));
                        }

                        sw.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\r",

                                                                 //40
                                                                 "1",
                                                                 //41
                                                                 vendita.MAGAZZINO,
                                                                 //122
                                                                 ADD_ZERO(cliente.ID.ToString()),
                                                                 //43
                                                                 vendita.ID.ToString(),
                                                                 //124
                                                                 string.Format("{0:yyyyMMdd}", vendita.creazione),
                                                                 //45
                                                                 vendita.ID.ToString(),
                                                                 //126
                                                                 string.Format("{0:yyyyMMdd}", vendita.creazione),
                                                                 //87
                                                                 string.Format("{0:HH.mm}", vendita.creazione),
                                                                 //78
                                                                 quantita.ToString().Replace(",", "."));

                        int count = 1;
                        foreach (var articolo in articoli)
                        {
                            sw.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\r",
                                "2",
                                string.Format("{0:yyyyMMdd}", vendita.creazione),
                                vendita.ID.ToString(),
                                count.ToString(),
                                articolo.CODICE_ARTICOLO,
                                articolo.QUANTITA);
                            count = count + 1;
                        }

                    }
                }
            }

        }
        private void createTXT_MOVIMENTAZIONE(string filename)
        {
            var dbName = filename;
            var path = Path.Combine("/sdcard/", dbName);

            using (FileStream aFile = new FileStream(path, FileMode.Create, FileAccess.Write))

            using (StreamWriter sw = new StreamWriter(aFile))
            {
                SQLite_Manager slm = new SQLite_Manager();

                string message = string.Empty;
                bool ok = false;

                var movimentazioni = slm.GET_ALL_MOVIMENTAZIONI(ref message, ref ok);


                if (ok)
                {
                    foreach (var movimentazione in movimentazioni)
                    {


                        decimal quantita = 0;


                        //Calcolo il peso della vendita
                        var articoli = slm.GET_ARTICOLIMOVIMENTAZIONI_BYCODE(movimentazione.ID_MOVIMENTAZIONE, ref message, ref ok);
                        foreach (var articolo in articoli)
                        {
                            quantita = quantita + (Int32.Parse(articolo.QUANTITA) * Decimal.Parse(articolo.PESO_UNITARIO));
                        }

                        sw.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\r",

                                                                 "1",
                                                                 movimentazione.CODICE_MAGAZZINO_PARTENZA,
                                                                 "09041457",
                                                                 movimentazione.ID_MOVIMENTAZIONE,
                                                                 string.Format("{0:yyyyMMdd}", movimentazione.DATA_CREAZIONE),
                                                                 movimentazione.ID_MOVIMENTAZIONE,
                                                                 string.Format("{0:yyyyMMdd}", movimentazione.DATA_CREAZIONE),
                                                                 string.Format("{0:HH.mm}", movimentazione.DATA_CREAZIONE),
                                                                 quantita.ToString().Replace(",", "."),
                                                                 movimentazione.CODICE_MAGAZZINO_ARRIVO
                                                                 );

                        int count = 1;
                        foreach (var articolo in articoli)
                        {
                            sw.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\r",
                                "2",
                                string.Format("{0:yyyyMMdd}", movimentazione.DATA_CREAZIONE),
                                movimentazione.ID_MOVIMENTAZIONE,
                                count.ToString(),
                                articolo.CODICE_ARTICOLO,
                                articolo.QUANTITA);
                            count = count + 1;
                        }

                    }
                }
            }

        }
        private bool CHECK_VENDITE_FOR_TXT()
        {
            string message = string.Empty;
            bool ok = false;

            SQLite_Manager slm = new SQLite_Manager();

            var vendite = slm.GET_ALL_VENDITE_SQLite(ref message, ref ok);

            //Se non ce nessuna vendita ritorno true
            if (vendite == null) return true;
            //Se non ce nessuna vendita ritorno true
            if (vendite.Count == 0) return true;

            foreach (var vendita in vendite)
            {
                var articoli = slm.GET_ARTICOLI_BYCODE(vendita.ID.ToString(), ref message, ref ok);
                if (articoli == null) return false;
                if (articoli.Count == 0) return false;

            }

            return true;
        }
        private bool CHECK_MOVIMENTAZIONI_FOR_TXT()
        {
            string message = string.Empty;
            bool ok = false;

            SQLite_Manager slm = new SQLite_Manager();

            var movimentazioni = slm.GET_ALL_MOVIMENTAZIONI(ref message, ref ok);

            //Se non ce nessuna vendita ritorno true
            if (movimentazioni == null) return true;
            //Se non ce nessuna vendita ritorno true
            if (movimentazioni.Count == 0) return true;


            foreach (var movimentazione in movimentazioni)
            {
                var articoli = slm.GET_ARTICOLIMOVIMENTAZIONI_BYCODE(movimentazione.ID_MOVIMENTAZIONE.ToString(), ref message, ref ok);

                if (articoli == null) return false;
                if (articoli.Count == 0) return false;
            }

            return true;
        }
        #endregion


    }
}