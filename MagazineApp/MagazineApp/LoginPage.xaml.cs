﻿using MagazineApp.Database.SQLite;
using MagazineApp.Database.SQLite.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MagazineApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        public void OnClick_BT_Login(object sender, EventArgs args)
        {
            string message = string.Empty;
            bool ok = false;

            SQLite_Manager slm = new SQLite_Manager();
            MySQL_Settings_SQLite settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);

            if (settings.DEVICE_PASSWORD == ED_PASSWORD.Text.Trim())
            {
                Navigation.PushAsync(new SettingPage());
            }
            else
            {
                DisplayAlert("Login Message", "PASSWORD NON CORRETTA.", "OK");
            }
        }
    }
}