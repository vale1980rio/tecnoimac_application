﻿
using AutoMapper;
using DevExpress.Mobile.Core.Containers;
using DevExpress.Mobile.DataGrid;
using DevExpress.Mobile.DataGrid.Theme;
using MagazineApp.Database.SQLite;
using MagazineApp.Database.SQLite.Objects;
using MagazineApp.DataGrid.Objects;
using MagazineApp.DataGrid.Repository;
using MagazineApp.Interfaces;
using MagazineApp.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MagazineApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingPage : CarouselPage
    {
        public SettingPage()
        {

            ThemeManager.ThemeName = Themes.Light;
            // Header customization.
            ThemeManager.Theme.HeaderCustomizer.BackgroundColor = Color.FromRgb(63, 112, 140);
            ThemeFontAttributes myFont = new ThemeFontAttributes("", ThemeFontAttributes.FontSizeFromNamedSize(NamedSize.Small), FontAttributes.Bold, Color.White);
            ThemeManager.Theme.HeaderCustomizer.Font = myFont;

            InitializeComponent();

            Carica_Dati();


        }
        public void Click_BT_FTP_Data(object sender, EventArgs args)
        {

            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            MySQL_Settings_SQLite mySQl_Settings = new MySQL_Settings_SQLite();

            slm.UPDATE_FTP_SETTINGS_SQLite(ED_FTP_SERVER.Text, ref message, ref ok);

            if (ok)
            {
                DependencyService.Get<IMessage>().ShortAlert("Dati FTP Salvati Correttamente.");
            }
            else
            {
                DependencyService.Get<IMessage>().ShortAlert(message);
            }
        }




        public void OnClick_BT_Salva(object sender, EventArgs args)
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            MySQL_Settings_SQLite mySQl_Settings = new MySQL_Settings_SQLite();

            mySQl_Settings.SERVER = ED_SERVER.Text;
            mySQl_Settings.PORT = ED_PORT.Text;
            mySQl_Settings.DATABASE = ED_DATABASE.Text;
            mySQl_Settings.USERID = ED_USERID.Text;
            mySQl_Settings.PASSWORD = ED_PASSWORD.Text;

            slm.ADD_MySQL_SETTINGS(mySQl_Settings, ref message, ref ok);


            if (ok)
            {
                DependencyService.Get<IMessage>().ShortAlert("Dati MySQL Salvati Correttamente.");
            }
            else
            {
                DependencyService.Get<IMessage>().ShortAlert(message);
            }
        }

        #region UTILITY

        private void Carica_Dati()
        {

            #region MySQL
            SQLite_Manager slm = new SQLite_Manager();
            string message = string.Empty;
            bool ok = false;
            MySQL_Settings_SQLite mySQl_Settings = slm.GET_MySQL_SETTINGS(ref message, ref ok);

            if (ok)
            {
                ED_SERVER.Text = mySQl_Settings.SERVER;
                if (!string.IsNullOrEmpty(mySQl_Settings.PORT))
                    ED_PORT.Text = mySQl_Settings.PORT.ToString();
                ED_DATABASE.Text = mySQl_Settings.DATABASE;
                ED_USERID.Text = mySQl_Settings.USERID;
                ED_PASSWORD.Text = mySQl_Settings.PASSWORD;
                ED_FTP_SERVER.Text = mySQl_Settings.FTP_SERVER;
            }
            else
            {
                DependencyService.Get<IMessage>().ShortAlert(message);
            }
            #endregion

            #region STAMPANTI

            DevExpressStampantiRepository model = new DevExpressStampantiRepository();
            grid.BindingContext = model;

            #endregion

            #region DEVICE_SETTINGS

            ED_DEVICE_NAME.Text = mySQl_Settings.DEVICE_NAME;

            ED_PASSWORD_DEVICE.Text = mySQl_Settings.DEVICE_PASSWORD;

            #endregion


        }

        #endregion



        public void Click_BT_Add_Printer_Data(object sender, EventArgs args)
        {
      
            string message = string.Empty;
            bool ok = false;
            Stampante_SQLite stampante_SQLite = new Stampante_SQLite();
            if (!string.IsNullOrEmpty(ED_DESCRIZIONE_PRINTER.Text.Trim())  && !string.IsNullOrEmpty(ED_DESCRIZIONE_2_PRINTER.Text.Trim()))
            {
                stampante_SQLite.DESCRIZIONE = ED_DESCRIZIONE_PRINTER.Text;
                stampante_SQLite.DESCRIZIONE_2 = ED_DESCRIZIONE_2_PRINTER.Text;
                
                SQLite_Manager slm = new SQLite_Manager();
                slm.ADD_STAMPANTI_SQLite(stampante_SQLite, ref message, ref ok);

                DevExpressStampantiRepository model = new DevExpressStampantiRepository();
                grid.BindingContext = model;

                DependencyService.Get<IMessage>().ShortAlert(message);

                ED_DESCRIZIONE_2_PRINTER.Text = string.Empty;
                ED_DESCRIZIONE_PRINTER.Text = string.Empty;


            }
            else
            {
                DependencyService.Get<IMessage>().ShortAlert("Nessun Nome o Descrizione Inserita");
            }
        }


        public void Click_BT_Add_Device_Data(object sender, EventArgs args)
        {
            string message = string.Empty;
            bool ok = false;

            string device_name = ED_DEVICE_NAME.Text;
            string password_device = ED_PASSWORD_DEVICE.Text;


            SQLite_Manager slm = new SQLite_Manager();
            slm.UPDATE_DEVICE_SETTINGS_SQLite(device_name, password_device, ref message, ref ok);

            if (ok)
            {
                DependencyService.Get<IMessage>().ShortAlert("Dati Device Salvati Correttamente.");
            }
            else
            {
                DependencyService.Get<IMessage>().ShortAlert(message);
            }

        }


        public void Click_BT_Delete_Printer_Data(object sender, EventArgs args)
        {



            //Rimuovo Dalla Griglia
            Stampante_DEVEXPRESS obj = (Stampante_DEVEXPRESS)grid.SelectedDataObject;
            if (obj != null)
            {
                MapperConfiguration configuration;
                configuration = new MapperConfiguration(a => { a.AddProfile(new Stampante_DEVEXPRESS__Stampante_SQLite_Profile()); });
                IMapper mapper;
                mapper = configuration.CreateMapper();
                Stampante_SQLite result = mapper.Map<Stampante_DEVEXPRESS, Stampante_SQLite>(obj);
                SQLite_Manager slm = new SQLite_Manager();
                string message = string.Empty;
                bool ok = false;

                slm.DELETE_STAMPANTE_SQLite(result, ref message, ref ok);


                DevExpressStampantiRepository model = new DevExpressStampantiRepository();
                grid.BindingContext = model;
            }
            else
            {
                DependencyService.Get<IMessage>().ShortAlert("Nessun Elemento Selezionato");
            }





        }
    }

}