﻿using Android.Graphics.Drawables;
using AutoMapper;
using DevExpress.Mobile.DataGrid;
using DevExpress.Mobile.DataGrid.Theme;
using MagazineApp.Database.MySQL;
using MagazineApp.Database.SQLite;
using MagazineApp.Database.SQLite.Objects;
using MagazineApp.DataGrid.Objects;
using MagazineApp.DataGrid.Repository;
using MagazineApp.Mappings;
using MagazineApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MagazineApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MovimentazionaManagerPage : CarouselPage
    {

      


        public MovimentazionaManagerPage()
        {
            InitializeComponent();
            Carica_Dati_Griglia();
            Paint_Grid();

        }

        //void OnCustomizeCell(CustomizeCellEventArgs e)
        //{
        //    try
        //    {
        //        if (e.FieldName == "ID_MOVIMENTAZIONE")
        //        {

        //            int index = e.RowHandle;

        //            var id = grid_movimentazioni.GetCellValue(index, "ID_MOVIMENTAZIONE");

        //            SQLite_Manager slm = new SQLite_Manager();

        //            string message = string.Empty;
        //            bool ok = false;
        //            int elements = 0;
        //            try
        //            {
        //                elements = slm.GET_ARTICOLIMOVIMENTAZIONI_BYCODE(id.ToString(), ref message, ref ok).Count;
        //            }
        //            catch
        //            {
        //                elements = 0;
        //            }


        //            if (elements == 0)
        //            {
        //                e.BackgroundColor = Xamarin.Forms.Color.PaleVioletRed;
        //            }
        //            else
        //            {
        //                e.BackgroundColor = Xamarin.Forms.Color.LightGreen;
        //            }
        //            e.Handled = true;
        //        }
        //    }
        //    catch { }
        //}

        #region GRID
        void Paint_Grid()
        {
            ThemeManager.ThemeName = Themes.Light;

            ThemeManager.Theme.HeaderCustomizer.BackgroundColor = Color.FromRgb(63, 112, 140);
            ThemeFontAttributes myFont = new ThemeFontAttributes("", ThemeFontAttributes.FontSizeFromNamedSize(NamedSize.Small), FontAttributes.Bold, Color.White);
            ThemeManager.Theme.HeaderCustomizer.Font = myFont;
        }

        private void Carica_Dati_Griglia()
        {
            DevExpressMagazziniRepository model_magazzini_partenza = new DevExpressMagazziniRepository();
            grid_magazzini_arrivo.BindingContext = model_magazzini_partenza;

            DevExpressMagazziniRepository model_magazzini_arrivo = new DevExpressMagazziniRepository();
            grid_magazzini_partenza.BindingContext = model_magazzini_arrivo;

            DevExpressMovimentazioniRepository model_movimentazioni = new DevExpressMovimentazioniRepository();
            grid_movimentazioni.BindingContext = model_movimentazioni;


        }
        #endregion

        #region BUTTONS
        public void OnClick_BT_Movimentazione_Crea(object sender, EventArgs args)
        {
            string message_ = string.Empty;
            bool ok_ = false;
            SQLite_Manager slm = new SQLite_Manager();

            MySQL_Settings_SQLite settings = slm.GET_MySQL_SETTINGS(ref message_, ref ok_);


            bool ok_FTP = CheckLanStatus.CheckHost(settings.SERVER, 3306);

            if (!ok_FTP)
            {
                DisplayAlert("ERRORE", "Server Non Raggiungibile.", "OK");
                return;
            }

            Magazzino_DEVEXPRESS magazzino_partenza = (Magazzino_DEVEXPRESS)grid_magazzini_partenza.SelectedDataObject;
            Magazzino_DEVEXPRESS magazzino_arrivo = (Magazzino_DEVEXPRESS)grid_magazzini_arrivo.SelectedDataObject;
            
            if (magazzino_arrivo.CODICE == magazzino_partenza.CODICE)
            {
                DisplayAlert("TRASFERIMENTO", "Selezionare Magazzini Diversi.", "OK");
                return;
            }
            
            MySQL_Manager msm = new MySQL_Manager();

            string message = "";
            bool ok = false;

            string ID = msm.GET_ID_MOVIMENTAZIONE_MYSQL(magazzino_partenza.ID.ToString(), magazzino_arrivo.ID.ToString(), ref message, ref ok);

            if (ok)
            {
                string message_2 = "";
                bool ok_2 = false;

                Movimentazione_SQLite Movimentazione_SQLite = new Movimentazione_SQLite();

                slm.INSERT_MAGAZZINI_MOVIMENTAZIONE(ref Movimentazione_SQLite, ID, magazzino_partenza.ID.ToString(), magazzino_partenza.CODICE, magazzino_partenza.DESCRIZIONE, magazzino_arrivo.ID.ToString(), magazzino_arrivo.CODICE, magazzino_arrivo.DESCRIZIONE, DateTime.Now, ref message_2, ref ok_2);


                DevExpressMovimentazioniRepository model_movimentazioni = new DevExpressMovimentazioniRepository();
                grid_movimentazioni.BindingContext = model_movimentazioni;

                slm.GET_ALL_MOVIMENTAZIONI(ref message, ref ok).Last();

                Navigation.PushAsync(new MovimentazionePage(slm.GET_ALL_MOVIMENTAZIONI(ref message, ref ok).Last()));
            }

        }

        public async void BT_MovimentazionePage_Click(object sender, EventArgs args)
        {

            Movimentazione_DEVEXPRESS obj = (Movimentazione_DEVEXPRESS)grid_movimentazioni.SelectedDataObject;

            if (obj == null)
            {
                await DisplayAlert("TRASFERIMENTO", "Nessun Trasferimento Selezionato.", "OK");
                return;
            }

            await Navigation.PushAsync(new MovimentazionePage(obj));
        }
        public void BT_Elimina_Click(object sender, EventArgs args)
        {
            try
            {
                Movimentazione_DEVEXPRESS obj = (Movimentazione_DEVEXPRESS)grid_movimentazioni.SelectedDataObject;


                if (obj == null)
                {
                    DisplayAlert("TRASFERIMENTO", "Nessun Trasferimento Selezionato.", "OK");
                    return;
                }


                #region MOVIMENTAZIONI

                string message = "NESSUN ERRORE";
                bool ok = false;


                SQLite_Manager slm = new SQLite_Manager();
                slm.DELETE_MOVIMENTAZIONE_SQLite(obj.ID.ToString(), ref message, ref ok);


                #endregion

                string message_2 = "NESSUN ERRORE";
                bool ok_2 = false;

                #region ARTICOLI
                slm.DELETE_ARTICOLOMOVIMENTAZIONE__BY_FK_ARTICOLO_SQLite(obj.ID.ToString(), ref message_2, ref ok_2);

                if ((ok) && (ok_2))
                {

                    DevExpressMovimentazioniRepository model_articoli = new DevExpressMovimentazioniRepository();
                    grid_movimentazioni.BindingContext = model_articoli;
                }
                else
                {
                    DisplayAlert("TRASFERIMENTO", "ERRORE CANCELLAZIONE ARTICOLI: " + message_2 + System.Environment.NewLine + "ERRORE CANCELLAZIONE TRASFERIMENTI: " + message, "OK");
                }
                #endregion
            }
            catch (Exception e)
            {
                DisplayAlert("TRASFERIMENTO", e.Message, "OK");

            }
        }

        #endregion


    }
}