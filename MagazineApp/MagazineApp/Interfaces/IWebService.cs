﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Interfaces
{
    public interface IWebService
    {
         string Print(byte[] file, string printername, string filename);
    }
}
