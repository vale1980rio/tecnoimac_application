﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace MagazineApp.Interfaces
{
    public interface ISave
    {
        Task SaveTextAsync(string filename, string contentType, MemoryStream s);
        Task ViewTextAsync(string filename, string contentType, MemoryStream s);
    }
}