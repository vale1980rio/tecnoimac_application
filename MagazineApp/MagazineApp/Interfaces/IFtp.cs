﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Interfaces
{

    public interface IFtpWebRequest
    {
        string upload(ref bool ok, string FtpUrl, string fileName, string UploadDirectory = "");

        string download(ref bool ok, string ftpSourceFilePath, string localDestinationFilePath);
		
		string append(ref bool ok, string FtpUrl, string fileName, string UploadDirectory = "");
 
    }
}
