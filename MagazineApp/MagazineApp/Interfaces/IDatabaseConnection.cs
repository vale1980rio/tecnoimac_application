﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Interfaces
{
    public interface IDatabaseConnection
    {
        SQLite.SQLiteConnection DbConn();
    }
}
