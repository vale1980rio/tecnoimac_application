﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MagazineApp.Interfaces
{
        public interface IIpAddressManager
        {
            string GetIpAddress();
        }
}
