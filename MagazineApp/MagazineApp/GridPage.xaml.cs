﻿using DevExpress.Mobile.DataGrid;
using DevExpress.Mobile.DataGrid.Theme;
using MagazineApp.DataGrid.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MagazineApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GridPage : ContentPage
	{
		public GridPage ()
		{
          
            ThemeManager.ThemeName = Themes.Light;

            // Header customization.
            ThemeManager.Theme.HeaderCustomizer.BackgroundColor = Color.FromRgb(63, 112, 140);
            ThemeFontAttributes myFont = new ThemeFontAttributes("",
                                        ThemeFontAttributes.FontSizeFromNamedSize(NamedSize.Default),
                                        FontAttributes.Bold, Color.White);
            ThemeManager.Theme.HeaderCustomizer.Font = myFont;
            
            InitializeComponent ();

          
        }


        void OnSwipeButtonClick(object sender, SwipeButtonEventArgs e)
        {

            //EDIT
            if (e.ButtonInfo.ButtonName == "LeftButton")
            {
                string orderDate = grid.GetCellValue(e.RowHandle, "Quantita").ToString();

                DisplayAlert("Alert from " + e.ButtonInfo.ButtonName, "Day: " + orderDate, "OK");
            }

            //DELETE
            if (e.ButtonInfo.ButtonName == "RightButton")
            {
                grid.DeleteRow(e.RowHandle);
            }
        }

        void OnSwipeButtonShowing(object sender, SwipeButtonShowingEventArgs e)
        {
            if ((!(Boolean)grid.GetCellValue(e.RowHandle, "Shipped"))
                && (e.ButtonInfo.ButtonName == "RightButton"))
            {
                e.IsVisible = false;
            }
        }


        //public async  void OnClick_BT_Indietro(object sender, EventArgs args)
        //{
        //    await Navigation.PushAsync(new HomePage());
        //}

        public async void OnClick_BT_Visualizza(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new ArticoloPage());
        }

    }
}