﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using UsingResxLocalization;
using Xamarin.Forms;

namespace MagazineApp
{
	public partial class App : Application
	{
        public static bool ImInLoadingView;


        public App()
        {
            //CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(CultureInfo.CurrentUICulture.ToString());
            //CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("fr-FR");
            this.MainPage = GeHomePage();
          
        }
      
        public static Page GeHomePage()
        {
            return new  NavigationPage(new HomePage());
        }
        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}


	}
}
