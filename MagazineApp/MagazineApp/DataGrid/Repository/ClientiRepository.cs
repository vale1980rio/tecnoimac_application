﻿
using MagazineApp.DataGrid.Objects;
using DevExpress.Mobile.Core.Containers;
using MagazineApp.Database.SQLite;

namespace MagazineApp.DataGrid.Repository
{
    public abstract class ClientiRepository
    {
        readonly DevExpress.Mobile.Core.Containers.BindingList<Cliente_DEVEXPRESS> clienti;

        public ClientiRepository()
        {
            this.clienti = new DevExpress.Mobile.Core.Containers.BindingList<Cliente_DEVEXPRESS>();
        }

        public DevExpress.Mobile.Core.Containers.BindingList<Cliente_DEVEXPRESS> Clienti
        {
            get { return clienti; }
        }
    }

    public class DevExpressClientiRepository : ClientiRepository
    {
        public DevExpressClientiRepository() : base()
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            BindingList<Cliente_DEVEXPRESS> Cliente_DEVEXPRESS = new BindingList<Cliente_DEVEXPRESS>();

            Cliente_DEVEXPRESS = slm.GET_ALL_CLIENTI_SQLite(ref message, ref ok);

            //Stampanti.Add(new Stampante_DEVEXPRESS());

            if (ok)
            {
                foreach (var v in Cliente_DEVEXPRESS)
                {
                    Clienti.Add(v);
                }
            }
        }
    }
}
