﻿
using System.Collections.Generic;

using MagazineApp.DataGrid.Objects;
using MagazineApp.Database.SQLite;


namespace MagazineApp.DataGrid.Repository
{
    public abstract class MagazziniRepository
    {
        public DevExpress.Mobile.Core.Containers.BindingList<Magazzino_DEVEXPRESS> magazzini;

        public MagazziniRepository()
        {
            this.magazzini = new DevExpress.Mobile.Core.Containers.BindingList<Magazzino_DEVEXPRESS>();
        }

        public DevExpress.Mobile.Core.Containers.BindingList<Magazzino_DEVEXPRESS> Magazzini
        {
          get { return magazzini; }
          set { this.Magazzini = magazzini; }
        }
    }

    public class DevExpressMagazziniRepository : MagazziniRepository
    {
        public DevExpressMagazziniRepository() : base()
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            List<Magazzino_DEVEXPRESS> Magazzini_DEVEXPRESS = new List<Magazzino_DEVEXPRESS>();

            Magazzini_DEVEXPRESS = slm.GET_ALL_MAGAZZINI(ref message, ref ok);

            if (ok)
            {

                foreach (var v in Magazzini_DEVEXPRESS)
                    Magazzini.Add(v);
            }

        }
    }
}
