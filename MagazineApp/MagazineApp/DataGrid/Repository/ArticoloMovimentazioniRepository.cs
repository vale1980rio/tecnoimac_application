﻿
using MagazineApp.DataGrid.Objects;
using MagazineApp.Database.SQLite;
using DevExpress.Mobile.Core.Containers;

namespace MagazineApp.DataGrid.Repository
{
    public abstract class ArticoloMovimentazioniRepository
    {
        public DevExpress.Mobile.Core.Containers.BindingList<Articolo_DEVEXPRESS> articolimovimentazioni;

        public ArticoloMovimentazioniRepository()
        {
            this.articolimovimentazioni = new DevExpress.Mobile.Core.Containers.BindingList<Articolo_DEVEXPRESS>();
        }

        public DevExpress.Mobile.Core.Containers.BindingList<Articolo_DEVEXPRESS> ArticoliMovimentazioni
        {
          get { return articolimovimentazioni; }
          set { this.ArticoliMovimentazioni = articolimovimentazioni; }
        }
    }

    public class DevExpressArticoloMovimentazioniRepository : ArticoloMovimentazioniRepository
    {
        public DevExpressArticoloMovimentazioniRepository(string id_movimentazione) : base()
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            BindingList<Articolo_DEVEXPRESS> Articoli_DEVEXPRESS = new BindingList<Articolo_DEVEXPRESS>();

            Articoli_DEVEXPRESS = slm.GET_ARTICOLIMOVIMENTAZIONI_BYCODE(id_movimentazione, ref message, ref ok);

            if (ok)
            {
                foreach (var v in Articoli_DEVEXPRESS)
                {
                    ArticoliMovimentazioni.Add(v);
                }
            }

        }
    }
}
