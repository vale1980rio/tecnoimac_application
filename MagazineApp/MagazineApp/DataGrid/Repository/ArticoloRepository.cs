﻿
using MagazineApp.DataGrid.Objects;
using MagazineApp.Database.SQLite;
using DevExpress.Mobile.Core.Containers;

namespace MagazineApp.DataGrid.Repository
{
    public abstract class ArticoloRepository
    {
        public DevExpress.Mobile.Core.Containers.BindingList<Articolo_DEVEXPRESS> articoli;

        public ArticoloRepository()
        {
            this.articoli = new DevExpress.Mobile.Core.Containers.BindingList<Articolo_DEVEXPRESS>();
        }

        public DevExpress.Mobile.Core.Containers.BindingList<Articolo_DEVEXPRESS> Articoli
        {
          get { return articoli; }
          set { this.Articoli = articoli; }
        }
    }

    public class DevExpressArticoliRepository : ArticoloRepository
    {
        public DevExpressArticoliRepository(string id_vendita) : base()
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            BindingList<Articolo_DEVEXPRESS> Articoli_DEVEXPRESS = new BindingList<Articolo_DEVEXPRESS>();

            Articoli_DEVEXPRESS = slm.GET_ARTICOLI_BYCODE(id_vendita,ref message, ref ok);

            if (ok)
            {
                foreach (var v in Articoli_DEVEXPRESS)
                {
                    Articoli.Add(v);
                }
            }

        }
    }
    
}
