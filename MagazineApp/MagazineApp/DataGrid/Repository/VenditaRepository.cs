﻿
using MagazineApp.DataGrid.Objects;
using DevExpress.Mobile.Core.Containers;
using MagazineApp.Database.SQLite;

namespace MagazineApp.DataGrid.Repository
{
    public abstract class VenditaRepository
    {
        readonly DevExpress.Mobile.Core.Containers.BindingList<Vendita_DEVEXPRESS> vendite;

        public VenditaRepository()
        {
            this.vendite = new DevExpress.Mobile.Core.Containers.BindingList<Vendita_DEVEXPRESS>();
        }

        public DevExpress.Mobile.Core.Containers.BindingList<Vendita_DEVEXPRESS> Vendite
        {
            get { return vendite; }
        }
    }

    public class DevExpressVenditeRepository : VenditaRepository
    {
        public DevExpressVenditeRepository() : base()
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            BindingList<Vendita_DEVEXPRESS> Vendita_DEVEXPRESS = new BindingList<Vendita_DEVEXPRESS>();

            Vendita_DEVEXPRESS = slm.GET_ALL_VENDITE_SQLite(ref message, ref ok);

            
            if (ok)
            {
                foreach (var v in Vendita_DEVEXPRESS)
                {
                    Vendite.Add(v);
                }
            }
        }




    }
}
