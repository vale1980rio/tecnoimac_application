﻿
using MagazineApp.DataGrid.Objects;
using MagazineApp.Database.SQLite;
using DevExpress.Mobile.Core.Containers;

namespace MagazineApp.DataGrid.Repository
{
    public abstract class MovimentazioniRepository
    {
        public DevExpress.Mobile.Core.Containers.BindingList<Movimentazione_DEVEXPRESS> movimentazioni;

        public MovimentazioniRepository()
        {
            this.movimentazioni = new DevExpress.Mobile.Core.Containers.BindingList<Movimentazione_DEVEXPRESS>();
        }

        public DevExpress.Mobile.Core.Containers.BindingList<Movimentazione_DEVEXPRESS> Movimentazioni
        {
            get { return movimentazioni; }
            set { this.Movimentazioni = movimentazioni; }
        }
    }

    public class DevExpressMovimentazioniRepository : MovimentazioniRepository
    {
        public DevExpressMovimentazioniRepository() : base()
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            BindingList<Movimentazione_DEVEXPRESS> Movimentazione_DEVEXPRESS = new BindingList<Movimentazione_DEVEXPRESS>();

            Movimentazione_DEVEXPRESS = slm.GET_ALL_MOVIMENTAZIONI( ref message, ref ok);

            if (ok)
            {
                foreach (var v in Movimentazione_DEVEXPRESS)
                {
                    Movimentazioni.Add(v);
                }
            }
        }
    }
}
