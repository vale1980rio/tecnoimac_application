﻿
using MagazineApp.DataGrid.Objects;
using DevExpress.Mobile.Core.Containers;
using MagazineApp.Database.SQLite;

namespace MagazineApp.DataGrid.Repository
{
    public abstract class StampantiRepository
    {
        readonly DevExpress.Mobile.Core.Containers.BindingList<Stampante_DEVEXPRESS> stampanti;

        public StampantiRepository()
        {
            this.stampanti = new DevExpress.Mobile.Core.Containers.BindingList<Stampante_DEVEXPRESS>();
        }

        public DevExpress.Mobile.Core.Containers.BindingList<Stampante_DEVEXPRESS> Stampanti
        {
            get { return stampanti; }
        }
    }

    public class DevExpressStampantiRepository : StampantiRepository
    {
        public DevExpressStampantiRepository() : base()
        {
            string message = string.Empty;
            bool ok = false;
            SQLite_Manager slm = new SQLite_Manager();
            BindingList<Stampante_DEVEXPRESS> Stampante_DEVEXPRESS = new BindingList<Stampante_DEVEXPRESS>();

            Stampante_DEVEXPRESS = slm.GET_ALL_STAMPANTE_SQLite(ref message, ref ok);

            //Stampanti.Add(new Stampante_DEVEXPRESS());

            if (ok)
            {
                foreach (var v in Stampante_DEVEXPRESS)
                {
                    Stampanti.Add(v);
                    
                }
            }
        }




    }
}
