﻿
using System.ComponentModel;

namespace MagazineApp.DataGrid.Objects
{
    public class Articolo_DEVEXPRESS : ModelObject
    {
        string id;
        string ean_128;
        string descrizione;
        string pz_conf;
        string ean_13;
        string cod_fornitore;
        string descrizione_fornitore;
        string ean_fornitore;
        string codice_articolo;
        string flag_x_carico;
        string peso_unitario;
        string sviluppo_articolo;
        string magazzino;
        string cod_brico;
        string settore;
        string fila;
        string pz_x_mq;
        string lastupdate;
        string quantita;
        string approvato;
        string fk_articolo_vendita;
        
        public Articolo_DEVEXPRESS(string id,string ean_128, string descrizione, string pz_conf, string ean_13, string cod_fornitore, string descrizione_fornitore, string ean_fornitore, string codice_articolo, string flag_x_carico, string peso_unitario, string sviluppo_articolo, string magazzino, string cod_brico, string settore, string fila, string pz_x_mq, string lastupdate, string quantita, string approvato,string fk_articolo_vendita)
        {
            this.id = id;
            this.ean_128 = ean_128;
            this.descrizione = descrizione;
            this.pz_conf = pz_conf;
            this.ean_13 = ean_13;
            this.cod_fornitore = cod_fornitore;
            this.descrizione_fornitore = descrizione_fornitore;
            this.ean_fornitore = ean_fornitore;
            this.codice_articolo = codice_articolo;
            this.flag_x_carico = flag_x_carico;
            this.peso_unitario = peso_unitario;
            this.sviluppo_articolo = sviluppo_articolo;
            this.magazzino = magazzino;
            this.cod_brico = cod_brico;
            this.settore = settore;
            this.fila = fila;
            this.pz_x_mq = pz_x_mq;
            this.lastupdate = lastupdate;
            this.quantita = quantita;
            this.approvato = approvato;
            this.fk_articolo_vendita = fk_articolo_vendita;
        }
        
        public Articolo_DEVEXPRESS()
        {

        }
        public string ID { get { return id; } set { if (id != value) { id = value; RaisePropertyChanged("ID"); } } }
        public string EAN_128 { get { return ean_128; } set { if (ean_128 != value) { ean_128 = value; RaisePropertyChanged("EAN_128"); } } }
        public string DESCRIZIONE { get { return descrizione; } set { if (descrizione != value) { descrizione = value; RaisePropertyChanged("DESCRIZIONE"); } } }
        public string PZ_CONF { get { return pz_conf; } set { if (pz_conf != value) { pz_conf = value; RaisePropertyChanged("PZ_CONF"); } } }
        public string EAN_13 { get { return ean_13; } set { if (ean_13 != value) { ean_13 = value; RaisePropertyChanged("EAN_13"); } } }
        public string COD_FORNITORE { get { return cod_fornitore; } set { if (cod_fornitore != value) { cod_fornitore = value; RaisePropertyChanged("COD_FORNITORE"); } } }
        public string DESCRIZIONE_FORNITORE { get { return descrizione_fornitore; } set { if (descrizione_fornitore != value) { descrizione_fornitore = value; RaisePropertyChanged("DESCRIZIONE_FORNITORE"); } } }
        public string EAN_FORNITORE { get { return ean_fornitore; } set { if (ean_fornitore != value) { ean_fornitore = value; RaisePropertyChanged("EAN_FORNITORE"); } } }
        public string CODICE_ARTICOLO { get { return codice_articolo; } set { if (codice_articolo != value) { codice_articolo = value; RaisePropertyChanged("CODICE_ARTICOLO"); } } }
        public string FLAG_X_CARICO { get { return flag_x_carico; } set { if (flag_x_carico != value) { flag_x_carico = value; RaisePropertyChanged("FLAG_X_CARICO"); } } }
        public string PESO_UNITARIO { get { return peso_unitario; } set { if (peso_unitario != value) { peso_unitario = value; RaisePropertyChanged("PESO_UNITARIO"); } } }
        public string SVILUPPO_ARTICOLO { get { return sviluppo_articolo; } set { if (sviluppo_articolo != value) { sviluppo_articolo = value; RaisePropertyChanged("SVILUPPO_ARTICOLO"); } } }
        public string MAGAZZINO { get { return magazzino; } set { if (magazzino != value) { magazzino = value; RaisePropertyChanged("MAGAZZINO"); } } }
        public string COD_BRICO { get { return cod_brico; } set { if (cod_brico != value) { cod_brico = value; RaisePropertyChanged("COD_BRICO"); } } }
        public string SETTORE { get { return settore; } set { if (settore != value) { settore = value; RaisePropertyChanged("SETTORE"); } } }
        public string FILA { get { return fila; } set { if (fila != value) { fila = value; RaisePropertyChanged("FILA"); } } }
        public string PZ_X_MQ { get { return pz_x_mq; } set { if (pz_x_mq != value) { pz_x_mq = value; RaisePropertyChanged("PZ_X_MQ"); } } }
        public string LASTUPDATE { get { return lastupdate; } set { if (lastupdate != value) { lastupdate = value; RaisePropertyChanged("LASTUPDATE"); } } }
        public string QUANTITA { get { return quantita; } set { if (quantita != value) { quantita = value; RaisePropertyChanged("QUANTITA"); } } }
        public string APPROVATO { get { return approvato; } set { if (approvato != value) { approvato = value; RaisePropertyChanged("APPROVATO"); } } }
        public string FK_ARTICOLO_VENDITA { get { return fk_articolo_vendita; } set { if (fk_articolo_vendita != value) { fk_articolo_vendita = value; RaisePropertyChanged("FK_ARTICOLO_VENDITA"); } } }
    }
}








