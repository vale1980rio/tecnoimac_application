﻿
using System;
using System.ComponentModel;

namespace MagazineApp.DataGrid.Objects
{
    public class Vendita_DEVEXPRESS : ModelObject
    {
        public int id;
        public string fk_Cliente;
        public string cliente;
        public string descrizione;
        public string indirizzo;
        public string magazzino;
        public DateTime creazione;

        public Vendita_DEVEXPRESS(int id, int id_mysql, string fk_Cliente, string cliente, string descrizione, string indirizzo, string magazzino, DateTime creazione)
        {
            this.id = id;
            this.fk_Cliente = fk_Cliente;
            this.cliente = cliente;
            this.descrizione = descrizione;
            this.indirizzo = indirizzo;
            this.magazzino = magazzino;
            this.creazione = creazione;
        }
        
        public Vendita_DEVEXPRESS()
        {

        }
        
        public int ID { get { return id ;} set { if  (id != value ) { id = value; RaisePropertyChanged("ID");} } }
        
        public string FK_CLIENTE { get { return fk_Cliente; } set { if (fk_Cliente != value) { fk_Cliente = value; RaisePropertyChanged("FK_CLIENTE"); } } }

        public string CLIENTE { get { return cliente; } set { if (cliente != value) { cliente = value; RaisePropertyChanged("CLIENTE"); } } }

        public string DESCRIZIONE { get { return descrizione; } set { if (descrizione != value) { descrizione = value; RaisePropertyChanged("DESCRIZIONE"); } } }

        public string INDIRIZZO { get { return indirizzo; } set { if (indirizzo != value) { indirizzo = value; RaisePropertyChanged("INDIRIZZO"); } } }

        public string MAGAZZINO { get { return magazzino; } set { if (magazzino != value) { magazzino = value; RaisePropertyChanged("MAGAZZINO"); } } }

        public DateTime CREAZIONE { get { return creazione; } set { if (creazione != value) { creazione = value; RaisePropertyChanged("CREAZIONE"); } } }




    }
}








