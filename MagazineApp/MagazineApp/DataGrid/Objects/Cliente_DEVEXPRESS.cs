﻿

using System.ComponentModel;

namespace MagazineApp.DataGrid.Objects
{
    public class Cliente_DEVEXPRESS : ModelObject
    {
        int id;
        string cliente;
        string indirizzo;
        string provincia;
        string comune;
        string cap;
        string ivacf;


        public Cliente_DEVEXPRESS(int id, string cliente, string indirizzo, string provincia,string comune,string cap,string ivacf)
        {
            this.id = id;
            this.cliente = cliente;
            this.indirizzo = indirizzo;
            this.provincia=provincia;
            this.comune= comune;
            this.cap=  cap;
            this.ivacf= ivacf;
        }
        
        public Cliente_DEVEXPRESS()
        {
            
        }

        public int ID
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        public string CLIENTE
        {
            get { return cliente; }
            set
            {
                if (cliente != value)
                {
                    cliente = value;
                    RaisePropertyChanged("CLIENTE");
                }
            }
        }

        public string INDIRIZZO
        {
            get { return indirizzo; }
            set
            {
                if (indirizzo != value)
                {
                    indirizzo = value;
                    RaisePropertyChanged("INDIRIZZO");
                }
            }
        }

        public string PROVINCIA
        {
            get { return provincia; }
            set
            {
                if (provincia != value)
                {
                    provincia = value;
                    RaisePropertyChanged("PROVINCIA");
                }
            }
        }

        public string COMUNE
        {
            get { return comune; }
            set
            {
                if (comune != value)
                {
                    comune = value;
                    RaisePropertyChanged("COMUNE");
                }
            }
        }

        public string CAP
        {
            get { return cap; }
            set
            {
                if (cap != value)
                {
                    cap = value;
                    RaisePropertyChanged("CAP");
                }
            }
        }

        public string IVACF
        {
            get { return ivacf; }
            set
            {
                if (ivacf != value)
                {
                    ivacf = value;
                    RaisePropertyChanged("IVACF");
                }
            }
        }
    }

}
