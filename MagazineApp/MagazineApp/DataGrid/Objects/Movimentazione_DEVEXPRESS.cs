﻿
using System;
using System.ComponentModel;

namespace MagazineApp.DataGrid.Objects
{
    public class Movimentazione_DEVEXPRESS : ModelObject
    {
        int id;
        string id_movimentazione;
        string fk_magazzino_partenza;
        string codice_magazzino_partenza;
        string descrizione_magazzino_partenza;
        string fk_magazzino_arrivo;
        string codice_magazzino_arrivo;
        string descrizione_magazzino_arrivo;
        DateTime data_creazione;



        public Movimentazione_DEVEXPRESS(int id, string id_movimentazione, string fk_magazzino_partenza, string codice_magazzino_partenza, string descrizione_magazzino_partenza,
            string fk_magazzino_arrivo, string codice_magazzino_arrivo, string descrizione_magazzino_arrivo, DateTime data_creazione)
         {
            this.id = id;
            this.id_movimentazione = id_movimentazione;
            this.fk_magazzino_partenza = fk_magazzino_partenza;
            this.codice_magazzino_partenza = codice_magazzino_partenza;
            this.descrizione_magazzino_partenza = descrizione_magazzino_partenza;
            this.fk_magazzino_arrivo = fk_magazzino_arrivo;
            this.codice_magazzino_arrivo = codice_magazzino_arrivo;
            this.descrizione_magazzino_arrivo = descrizione_magazzino_arrivo;
            this.data_creazione = data_creazione;
        }
        
        public Movimentazione_DEVEXPRESS()
        {

        }
        
        public int ID { get { return id ;} set { if  (id != value ) { id = value; RaisePropertyChanged("ID");} } }

        public string ID_MOVIMENTAZIONE { get { return id_movimentazione; } set { if (id_movimentazione != value) { id_movimentazione = value; RaisePropertyChanged("ID_MOVIMENTAZIONE"); } } }

        public string FK_MAGAZZINO_PARTENZA { get { return fk_magazzino_partenza; } set { if (fk_magazzino_partenza != value) { fk_magazzino_partenza = value; RaisePropertyChanged("FK_MAGAZZINO_PARTENZA"); } } }

        public string CODICE_MAGAZZINO_PARTENZA { get { return codice_magazzino_partenza; } set { if (codice_magazzino_partenza != value) { codice_magazzino_partenza = value; RaisePropertyChanged("CODICE_MAGAZZINO_PARTENZA"); } } }

        public string DESCRIZIONE_MAGAZZINO_PARTENZA { get { return descrizione_magazzino_partenza; } set { if (descrizione_magazzino_partenza != value) { descrizione_magazzino_partenza = value; RaisePropertyChanged("DESCRIZIONE_MAGAZZINO_PARTENZA"); } } }

        public string FK_MAGAZZINO_ARRIVO { get { return fk_magazzino_arrivo; } set { if (fk_magazzino_arrivo != value) { fk_magazzino_arrivo = value; RaisePropertyChanged("FK_MAGAZZINO_ARRIVO"); } } }

        public string CODICE_MAGAZZINO_ARRIVO { get { return codice_magazzino_arrivo; } set { if (codice_magazzino_arrivo != value) { codice_magazzino_arrivo = value; RaisePropertyChanged("CODICE_MAGAZZINO_ARRIVO"); } } }

        public string DESCRIZIONE_MAGAZZINO_ARRIVO { get { return descrizione_magazzino_arrivo; } set { if (descrizione_magazzino_arrivo != value) { descrizione_magazzino_arrivo = value; RaisePropertyChanged("DESCRIZIONE_MAGAZZINO_ARRIVO"); } } }

        public DateTime DATA_CREAZIONE { get { return data_creazione; } set { if (data_creazione != value) { data_creazione = value; RaisePropertyChanged("DATA_CREAZIONE"); } } }


    }
}








