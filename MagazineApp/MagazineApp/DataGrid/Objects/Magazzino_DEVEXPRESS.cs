﻿
using System.ComponentModel;

namespace MagazineApp.DataGrid.Objects
{
    public class Magazzino_DEVEXPRESS : ModelObject
    {
        int id;
        int idmagazzino;
        string rand;
        string codice;
        string descrizione;
        string used;


        public Magazzino_DEVEXPRESS(int id, int idmagazzino, string rand, string codice, string descrizione, string used)
        {
            this.id = id;
            this.idmagazzino = idmagazzino;
            this.rand = rand;
            this.codice = codice;
            this.descrizione = descrizione;
            this.used = used;
         
        }

        public Magazzino_DEVEXPRESS()
        {

        }

        public int ID { get { return id; } set { if (id != value) { id = value; RaisePropertyChanged("ID"); } } }

        public int IDMAGAZZINO { get { return idmagazzino; } set { if (idmagazzino != value) { idmagazzino = value; RaisePropertyChanged("IDMAGAZZINO"); } } }

        public string RAND { get { return rand; } set { if (rand != value) { rand = value; RaisePropertyChanged("RAND"); } } }

        public string CODICE { get { return codice; } set { if (codice != value) { codice = value; RaisePropertyChanged("CODICE"); } } }

        public string DESCRIZIONE { get { return descrizione; } set { if (descrizione != value) { descrizione = value; RaisePropertyChanged("DESCRIZIONE"); } } }

        public string USED { get { return used; } set { if (used != value) { used = value; RaisePropertyChanged("USED"); } } }


    }
}








