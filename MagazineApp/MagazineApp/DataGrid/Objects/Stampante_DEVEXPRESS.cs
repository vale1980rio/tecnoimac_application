﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MagazineApp.DataGrid.Objects
{
    public class Stampante_DEVEXPRESS : ModelObject
    {
        int id;
        string ip;
        string porta;
        string descrizione;
        string descrizione_2;

        public Stampante_DEVEXPRESS(int id, string ip, string porta, string descrizione, string descrizione_2)
        {
            this.id = id;
            this.ip = ip;
            this.porta = porta;
            this.descrizione = descrizione;
            this.descrizione_2 = descrizione_2;
        }


        public Stampante_DEVEXPRESS()
        {
            
        }

        public int ID
        {
            get { return id; }
            set
            {
                if (id != value)
                {
                    id = value;
                    RaisePropertyChanged("ID");
                }
            }
        }

        public string IP
        {
            get { return ip; }
            set
            {
                if (ip != value)
                {
                    ip = value;
                    RaisePropertyChanged("IP");
                }
            }
        }

        public string PORTA
        {
            get { return porta; }
            set
            {
                if (porta != value)
                {
                    porta = value;
                    RaisePropertyChanged("PORTA");
                }
            }
        }

        public string DESCRIZIONE
        {
            get { return descrizione; }
            set
            {
                if (descrizione != value)
                {
                    descrizione = value;
                    RaisePropertyChanged("DESCRIZIONE");
                }
            }
        }
        public string DESCRIZIONE_2
        {
            get { return descrizione_2; }
            set
            {
                if (descrizione_2 != value)
                {
                    descrizione_2 = value;
                    RaisePropertyChanged("DESCRIZIONE_2");
                }
            }
        }


    }

   
}
