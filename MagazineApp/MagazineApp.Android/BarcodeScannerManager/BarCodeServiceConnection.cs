﻿using System;
using Android.Content;
using Android.OS;
using App.Dsic.Barcodetray;


namespace MagazineApp.Droid.BarcodeScannerManager
{
    public class BarCodeServiceConnection : Java.Lang.Object, IServiceConnection
    {
        IBarcodeInterface barcodeInterface = null;
        
        public new void Dispose()
        {
            base.Dispose();
        }

        public IBarcodeInterface GetService()
        {
            return barcodeInterface;
        }
        
        public void OnServiceDisconnected(ComponentName name)
        {
            barcodeInterface = null;
        }

        public void OnServiceConnected(ComponentName name, IBinder service)
        {
            barcodeInterface = IBarcodeInterfaceStub.AsInterface(service);
            //ogni volta che si connette lo mette INTENT_INTENT
            barcodeInterface.SetRecvType(0);
        }
    }
}