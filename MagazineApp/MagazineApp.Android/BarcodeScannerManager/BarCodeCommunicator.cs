﻿using System;

using Android.Content;
using App.Dsic.Barcodetray;
using MagazineApp.Droid.BarcodeScannerManager;

using Xamarin.Forms;


namespace MagazineApp.Droid.BarcodeScannerManager
{
    public class BarCodeScanner
    {
        IBarcodeInterface barcodeInterface;
        BarCodeServiceConnection serviceConn;

        public void Init(Context context)
        {
            serviceConn = new BarCodeServiceConnection();
            var intent = new Intent("app.dsic.barcodetray.IBarcodeInterface");
            context.StartService(intent);
            context.BindService(intent, serviceConn, Bind.AutoCreate);
        }
        

        public bool IsScanEnable()
        {
            bool test = false;

            barcodeInterface = serviceConn.GetService();

            if (barcodeInterface != null)
            {
                try
                {
                    test = barcodeInterface.IsScanEnable();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Print(string.Format("Error: {0}", ex.Message));
                    test = false;
                }
            }
            return test;
        }

        public void Set_INTENT_Type()
        {
            
            barcodeInterface = serviceConn.GetService();

            if (barcodeInterface != null)
            {
                try
                {
                    barcodeInterface.SetRecvType(0);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Print(string.Format("Error: {0}", ex.Message));
                }
            }
        }

        public void Set_KEYBOARD_Type()
        {
            barcodeInterface = serviceConn.GetService();

            if (barcodeInterface != null)
            {
                try
                {
                    barcodeInterface.SetRecvType(1);
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Print(string.Format("Error: {0}", ex.Message));
                }
            }
        }
    }


}