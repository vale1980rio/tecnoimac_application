﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using MagazineApp;

namespace MagazineApp.Droid.BarcodeScannerManager
{
    public class MyBroadCastReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
           string code =  intent.GetStringExtra("EXTRA_BARCODE_DECODED_DATA");
           MessagingCenter.Send<string, string>("MyApp", "NotifyMsg", code);
        }
    }
}