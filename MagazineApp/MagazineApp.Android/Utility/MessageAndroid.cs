﻿using Android.App;
using Android.Widget;
using MagazineApp.Interfaces;
using MagazineApp.Droid.Utility;

[assembly: Xamarin.Forms.Dependency(typeof(MessageAndroid))]
namespace MagazineApp.Droid.Utility
{
    public class MessageAndroid : IMessage
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}