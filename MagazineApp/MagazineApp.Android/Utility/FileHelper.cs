﻿
using System.IO;

using MagazineApp.Droid.Utility;
using MagazineApp.Interfaces;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileHelper))]
namespace MagazineApp.Droid.Utility
{ 
    public class FileHelper : IDatabaseConnection
    {
        public SQLiteConnection DbConn()
        {
            var dbName = "Magazzino.db3";
            //var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
            //Metto il file nella root principale per poterlo manipolare esternamente
            var path = Path.Combine("/sdcard/", dbName);
            return new SQLiteConnection(path);
        }

       
    }
}