﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using MagazineApp.Droid.Utility;
using MagazineApp.Interfaces;
using Xamarin.Forms;



#region Copyright Syncfusion Inc. 2001-2015.
// Copyright Syncfusion Inc. 2001-2015. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion


[assembly:  Xamarin.Forms.Dependency (typeof (SaveAndroid))]
namespace MagazineApp.Droid.Utility
{
    public class SaveAndroid : ISave
    {
        public async Task SaveTextAsync(string fileName, String contentType, MemoryStream s)
        {
            string root = fileName;
           
            Java.IO.File myDir = new Java.IO.File(root);
            myDir.Mkdir();

            Java.IO.File file = new Java.IO.File(fileName);

            if (file.Exists()) file.Delete();

            try
            {
                FileOutputStream outs = new FileOutputStream(file);
                outs.Write(s.ToArray());

                outs.Flush();
                outs.Close();

            }
            catch (Exception e)
            {

            }
        }

        public async Task ViewTextAsync(string fileName, String contentType, MemoryStream s)
        {
            string root = fileName;

            Java.IO.File myDir = new Java.IO.File(root);
            myDir.Mkdir();

            Java.IO.File file = new Java.IO.File(fileName);

            if (file.Exists()) file.Delete();

            try
            {
                FileOutputStream outs = new FileOutputStream(file);
                outs.Write(s.ToArray());

                outs.Flush();
                outs.Close();

            }
            catch (Exception e)
            {

            }

            if (file.Exists())
            {
                Android.Net.Uri path = Android.Net.Uri.FromFile(file);
                string extension = Android.Webkit.MimeTypeMap.GetFileExtensionFromUrl(Android.Net.Uri.FromFile(file).ToString());
                string mimeType = Android.Webkit.MimeTypeMap.Singleton.GetMimeTypeFromExtension(extension);
                Intent intent = new Intent(Intent.ActionView);
                intent.SetDataAndType(path, mimeType);
                Forms.Context.StartActivity(Intent.CreateChooser(intent, "Choose App"));
            }
        }
    }
}