﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MagazineApp.Droid.Utility;
using MagazineApp.Droid.PrintService;
using MagazineApp.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(Webservice_ANDROID))]
namespace MagazineApp.Droid.Utility
{
    public class Webservice_ANDROID : IWebService
    {
        MagazineApp.Droid.PrintService.PrintService client = new PrintService.PrintService();
     
        public string Print(byte[] file, string printername, string filename)
        {
            return  client.Print(file,printername,filename);
        }
    }
}