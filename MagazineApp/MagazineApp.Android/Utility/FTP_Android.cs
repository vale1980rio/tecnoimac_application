﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MagazineApp.Droid.Utility;
using MagazineApp.Interfaces;
using MagazineApp.Database.SQLite;

[assembly: Xamarin.Forms.Dependency(typeof(FTP))]
namespace MagazineApp.Droid.Utility
{

    class FTP : IFtpWebRequest
    {
        public FTP()
        {

        }

        public string upload(ref bool ok, string FtpUrl, string fileName, string UploadDirectory = "")
        {
            try
            {

                string PureFileName = new FileInfo(fileName).Name;
                String uploadUrl = String.Format("FTP://{0}{1}/{2}", FtpUrl, UploadDirectory, PureFileName);
                FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(uploadUrl);
                req.Proxy = null;
                req.Method = WebRequestMethods.Ftp.UploadFile;


                byte[] data = File.ReadAllBytes(fileName);
                req.ContentLength = data.Length;
                Stream stream = req.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Close();
                FtpWebResponse res = (FtpWebResponse)req.GetResponse();
                ok = true;
                return "File Caricato Sul Server.";
                
            }
            catch (Exception err)
            {
                ok = false;
                return err.ToString();
            }
        }

        public string download(ref bool ok, string ftpSourceFilePath, string localDestinationFilePath)
        {
            try
            {
                int bytesRead = 0;
                byte[] buffer = new byte[2048];

                if (File.Exists(localDestinationFilePath))
                {
                    File.Delete(localDestinationFilePath);
                }
                FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(ftpSourceFilePath);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                Stream reader = request.GetResponse().GetResponseStream();
                FileStream fileStream = new FileStream(localDestinationFilePath, FileMode.Create);

                while (true)
                {
                    bytesRead = reader.Read(buffer, 0, buffer.Length);

                    if (bytesRead == 0)
                        break;

                    fileStream.Write(buffer, 0, bytesRead);
                }
                fileStream.Close();
                FtpWebResponse res = (FtpWebResponse)request.GetResponse();
                ok = true;
                string resultato = res.StatusDescription;




                res.Dispose();
                fileStream.Dispose();
                return resultato;
            }
            catch (Exception err)
            {
                ok = false;
                return err.ToString();
            }

        }


        public string append(ref bool ok, string FtpUrl, string fileName, string UploadDirectory = "")
        {
            try
            {
                string PureFileName = new FileInfo(fileName).Name;
                String uploadUrl = String.Format("{0}{1}/{2}", FtpUrl, UploadDirectory, PureFileName);
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(uploadUrl);
                request.Method = WebRequestMethods.Ftp.AppendFile;

                byte[] data = File.ReadAllBytes("/sdcard/"+fileName);
                request.ContentLength = data.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(data, 0, data.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();

                SQLite_Manager slm = new SQLite_Manager();
                slm.RESET();

                return "File Caricato Sul Server.";
            }
            catch(Exception ext)
            {
                ok =  false;
                return "Errore: Nessun File Caricato Sul Server.";
            }
        }

     
    }
}

