﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;
using Android.Webkit;
using Xamarin.Forms;
using MagazineApp.Droid.BarcodeScannerManager;

namespace MagazineApp.Droid
{
    [Activity(Label = "MagazineApp", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {



        BarCodeScanner barCodeScanner = new BarCodeScanner();
        protected override void OnCreate(Bundle bundle)

        {
         
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);   
                
            global::DevExpress.Mobile.Forms.Init();
            global::Xamarin.Forms.Forms.Init(this, bundle);
            
            MyBroadCastReceiver myBroadCastReceiver = new MyBroadCastReceiver();
            this.RegisterReceiver(myBroadCastReceiver, new IntentFilter("app.dsic.barcodetray.BARCODE_BR_DECODING_DATA"));
            
            LoadApplication(new App());
        }
        //public override void OnBackPressed()
        //{
        //    return;
        //}
        protected override void OnStart()
        {
            var contex = Forms.Context;
            barCodeScanner.Init(contex);
            base.OnStart();
        }
        
        protected override void OnPause()
        {
            barCodeScanner.Set_KEYBOARD_Type();
            base.OnPause();
        }
        protected override void OnStop()
        {
            barCodeScanner.Set_KEYBOARD_Type();
            base.OnStop();
        }
        protected override void OnDestroy()
        {
            barCodeScanner.Set_KEYBOARD_Type();
            base.OnDestroy();
        }
    }
}

